<div class="container homeSlide" id="slide-3">
    <div class="bcg"
        data-center="background-position: 50% 0px;"
        data-top-bottom="background-position: 50% -100px;"
        data-bottom-top="background-position: 50% 100px;"
        data-anchor-target="#slide-3"
        style="background-image:url('<?php echo types_render_field( "parallax-image-3", array( "output" => "raw" ) ) ?>')"
    >
        <div class="hsContainer">
            <div class="hsContent">
                <div class="row">
                    <div class="span12">
                        <div class="info-boxes">
                            <?php
                                $infobox_1_title = types_render_field( "info-box-1-title", array( "output" => "raw" ) );
                                $infobox_1_content = types_render_field( "info-box-1-content", array( "output" => "html" ) );
                                $infobox_1_image = types_render_field( "info-box-1-image", array( "output" => "raw" ) );
                                $infobox_2_title = types_render_field( "info-box-2-title", array( "output" => "raw" ) );
                                $infobox_2_content = types_render_field( "info-box-2-content", array( "output" => "html" ) );
                                $infobox_2_image = types_render_field( "info-box-2-image", array( "output" => "raw" ) );
                                $infobox_3_title = types_render_field( "info-box-3-title", array( "output" => "raw" ) );
                                $infobox_3_content = types_render_field( "info-box-3-content", array( "output" => "html" ) );
                                $infobox_3_image = types_render_field( "info-box-3-image", array( "output" => "raw" ) );

                                if(!empty($infobox_1_title) || !empty($infobox_1_content) || !empty($infobox_1_image)){
                                    ?>
                                    <div class="info-box ibox-1">
                                        <img src="<?php echo $infobox_1_image; ?>" alt="<?php echo $infobox_1_title; ?> Image" />
                                        <div class="info-box-1-content">
                                            <h4>
                                                <?php echo $infobox_1_title; ?>
                                            </h4>
                                            <?php echo $infobox_1_content; ?>
                                            <!--<a class="btn btn-default" href="/<?php echo seoUrl($infobox_1_title)?>">Read More about <?php echo $infobox_1_title; ?></a>-->
                                        </div>
                                    </div>
                                    <?php
                                }

                                if(!empty($infobox_2_title) || !empty($infobox_2_content) || !empty($infobox_2_image)){
                                    ?>
                                    <div class="info-box ibox-2">
                                        <img src="<?php echo $infobox_2_image; ?>" alt="<?php echo $infobox_2_title; ?> Image" />
                                        <div class="info-box-2-content">
                                            <h4>
                                                <?php echo $infobox_2_title; ?>
                                            </h4>
                                            <?php echo $infobox_2_content; ?>
                                            <!--<a class="btn btn-default" href="/<?php echo seoUrl($infobox_2_title)?>">Read More about <?php echo $infobox_2_title; ?></a>-->
                                        </div>
                                    </div>
                                    <?php
                                }

                                if(!empty($infobox_3_title) || !empty($infobox_3_content) || !empty($infobox_3_image)){
                                    ?>
                                    <div class="info-box ibox-3">
                                        <img src="<?php echo $infobox_3_image; ?>" alt="<?php echo $infobox_3_title; ?> Image" />
                                        <div class="info-box-3-content">
                                            <h4>
                                                <?php echo $infobox_3_title; ?>
                                            </h4>
                                            <?php echo $infobox_3_content; ?>
                                            <!--<a class="btn btn-default" href="/<?php echo seoUrl($infobox_3_title)?>">Read More about <?php echo $infobox_3_title; ?></a>-->
                                        </div>
                                    </div>
                                    <?php
                                }
                            ?>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>