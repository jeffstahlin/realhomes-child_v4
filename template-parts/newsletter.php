<div class="newsletter-form homeSlide" id="slide-1">
	<div class="bcg" style="background: white;">
    <div class="hsContainer">
      <div class="hsContent">
				<h3>GET THE LATEST ON PROPERTY NEWS</h3>
				<?php echo do_shortcode('[newsletter_signup_form id=0]'); ?>
			</div>
		</div>
	</div>
</div>