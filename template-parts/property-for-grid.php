<article class="property-item clearfix">
    <?php
        $the_images = show_property_data('gallery-images');
        if( !empty($the_images) ){
            ?>
            <figure>
                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                    <div class="home-property-image" style="background: url(<?php echo $the_images[0]['url']; ?>); width: 244px; height: 180px; display: block; background-size: 100%; background-repeat: no-repeat;">

                    </div>
                </a>
                <figcaption>
                    <?php
                        echo show_property_data('status');
                    ?>
                </figcaption>
            </figure>
        <?php
        }
    ?>

    <h4>
        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
            <?php

                $property_title = the_title('', '', false);
                $pt = preg_replace('/(.+?)_\d{1,9}$/', '$1', $property_title);
                $utf8_pt = \ForceUTF8\Encoding::fixUTF8($pt);
                echo substr($utf8_pt, 0, 35) . '...';
            ?>
        </a>
        <p>
            <?php
                echo 'Ref: BC' . $post->ID;
            ?>
        </p>
    </h4>
    <p>
        <?php
            $description = show_property_data('description');
            echo substr($description, 0, 100) . '...';
        ?>
        <br>
        <a class="more-details" href="<?php the_permalink() ?>">
            <?php _e('More Details ','framework'); ?>
            <i class="fa fa-caret-right"></i>
        </a>
    </p>
    <span>
        <?php
            echo show_property_data('price');
        ?>
    </span>
</article>