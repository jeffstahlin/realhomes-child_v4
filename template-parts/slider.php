<?php
global $slider_properties;
$slider_properties = array();

$slider_args = array(
    'post_type' => 'property',
    'posts_per_page' => -1,
    'meta_query' => array(
        array(
            'key' => 'REAL_HOMES_add_in_slider',
            'value' => 'yes',
            'compare' => 'LIKE'
        )
    )
);

$slider_query = new WP_Query( $slider_args );

if($slider_query->have_posts()){
    ?>
    <div id="slide-2" class="homeSlide">
        <div class="hsContainer">
            <div class="hsContent">
                <!-- Slider -->
                <div id="home-flexslider" class="clearfix">
                    <div class="flexslider">
                        <ul class="slides">
                            <?php
                            while ( $slider_query->have_posts() ) :
                                $slider_query->the_post();
                                $slider_properties[] = intval($post->ID);
                                /* $slider_image_id = get_post_meta( $post->ID, 'REAL_HOMES_slider_image', true ); // commented for slider image fix */
                                $temp_slider_image_id = get_post_meta( $post->ID, 'REAL_HOMES_slider_image' );
                                $slider_image_id = array_pop( $temp_slider_image_id );
                                if($slider_image_id){
                                    $slider_image_url = wp_get_attachment_url($slider_image_id);
                                    ?>
                                    <li class="bcg"
                                        data-center="background-position: 50% 0px;"
                                        data-top-bottom="background-position: 50% -100px;"
                                        data-bottom-top="background-position: 50% 100px;"
                                        data-anchor-target="#slide-2"
                                        style="background-image:url('<?php echo $slider_image_url; ?>')"
                                    >
                                        <div class="desc-wrap">
                                            <div class="slide-description">
                                                <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                                <p><?php framework_excerpt(15); ?></p>
                                                <span><?php property_price(); ?></span>
                                                <a href="<?php the_permalink(); ?>" class="know-more"><?php _e('Know More','framework'); ?></a>
                                            </div>
                                        </div>
                                        <a href="<?php the_permalink(); ?>"><img src="<?php echo $slider_image_url; ?>" alt="<?php the_title(); ?>"></a>
                                    </li>
                                    <?php
                                }
                            endwhile;
                            wp_reset_query();
                            ?>
                        </ul>
                    </div>
                </div><!-- End Slider -->
            </div>
        </div>
    </div>
    <?php
}else{
    get_template_part('banners/default_page_banner');
}
?>