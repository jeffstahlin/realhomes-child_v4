<?php
	global $theme_search_url;
	$theme_search_url = get_option('theme_search_url');

	global $theme_search_fields;
	$theme_search_fields= get_option('theme_search_fields');
?>

<div class="widget property-searchform search homeSlide" id="slide-0" role="search">

	<div class="bcg" style="background-image:url('/wp-content/uploads/2014/05/seaview3-bigstock-1.jpg');">

    <div class="hsContainer">
      <div class="hsContent">

				<div class="property-search-box">
				<!-- <form class="advance-search-form clearfix" action="<?php global $theme_search_url; echo $theme_search_url; ?>" method="get"> -->
					<form class="advance-search-form clearfix" action="<?php bloginfo('url');  ?>" method="get">
						<div class="search-form-top">
							<h3 class="search-form-title">SEARCH</h3>
							<input id="autocomplete-location" type="search" name="s" id="s" placeholder="Search by Location" />
							<input type="submit" alt="Search" value="&#10004;" class="btn btn-success" />
						</div>
						<div class="clearfix"></div>
						<div id="slider-range"></div>
						<input type="hidden" id="min-price" name="min-price" style="border:0; color:#f6931f; font-weight:bold;">
						<input type="hidden" id="max-price" name="max-price" style="border:0; color:#f6931f; font-weight:bold;">
					</form>
				</div>
				<div class="property-results-box"></div>

			</div>
		</div>

	</div><!-- .search -->
</div>