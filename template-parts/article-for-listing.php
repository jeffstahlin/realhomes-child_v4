<?php
  global $post;
    $format = get_post_format();
    if( false === $format ) {
        $format = 'standard';
    }
?>

<?php
    if( is_search() ) {
        ?>
        <div class="span6 ">
            <article class="property-item clearfix">
                <h4>
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                        <?php

                          $property_title = the_title('', '', false);
                          $pt = preg_replace('/(.+?)_\d{1,9}$/', '$1', $property_title);
                          $utf8_pt = \ForceUTF8\Encoding::fixUTF8($pt);
                          echo $utf8_pt;

                        ?>
                    </a>
                    <span class="pull-right">
                        <?php
                            echo 'Ref: BC' . $post->ID;
                        ?>
                    </span>
                </h4>

                <?php
                $the_images = show_property_data('gallery-images');
                if( !empty($the_images) ){
                    ?>
                    <figure>
                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                            <div class="home-property-image" style="background: url(<?php echo $the_images[0]['url']; ?>); width: 244px; height: 180px; display: block; background-size: 100%; background-repeat: no-repeat;">

                            </div>
                        </a>
                        <figcaption>
                            <?php
                              echo show_property_data('status');
                            ?>
                        </figcaption>
                    </figure>
                <?php
                }
                ?>
                <div class="detail">
                    <h5 class="price">
                        <?php
                            echo show_property_data('price');
                        ?>
                    </h5>
                    <p>
                        <?php
                            $description = show_property_data('description');
                            echo substr($description, 0, 100) . '...';
                        ?>
                    </p>
                    <a class="more-details" href="<?php the_permalink() ?>"><?php _e('More Details ','framework'); ?><i class="fa fa-caret-right"></i></a>
                </div>
                <div class="property-meta">
                    <?php get_template_part('property-details/property-metas'); ?>
                </div>
            </article>
        </div>
        <?php
    } else {
        ?>
        <article <?php post_class(); ?>>
            <header>
                <h3 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                <div class="post-meta <?php echo $format; ?>-meta thumb-<?php echo has_post_thumbnail()?'exist':'not-exist'; ?>">
                    <span><?php _e('Posted on', 'framework'); ?> <span class="date"> <?php the_time('F d, Y'); ?></span></span>
                    <span><?php _e('by', 'framework'); ?> <span class="author-link"><?php the_author_posts_link() ?></span> <?php _e('in', 'framework'); ?> <?php the_category(', '); ?> </span>
                </div>
            </header>
            <?php get_template_part( "post-formats/$format" ); ?>
            <p><?php framework_excerpt(40);  ?></p>
            <a class="real-btn" href="<?php the_permalink(); ?>"><?php _e('Read more', 'framework'); ?></a>
        </article>
        <?php
    }
?>
