<?php
    // Banner Image
    $banner_image_path = "";
    $banner_image_id = get_post_meta( $post->ID, 'REAL_HOMES_page_banner_image', true );
    if($banner_image_id){
        $banner_image_path = wp_get_attachment_url($banner_image_id);
    }else{
        $banner_image_path = get_default_banner();
    }

    ?>

    <div class="page-head" style="background-repeat: no-repeat;background-position: center top;background-image: url('<?php echo $banner_image_path; ?>'); ">
        <div class="container">
            <div class="wrap clearfix">
                <h1 class="page-title"><span><?php _e('Property Search ','framework'); ?></span></h1>
            </div>
        </div>
    </div><!-- End Page Head -->