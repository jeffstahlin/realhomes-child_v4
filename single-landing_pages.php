<?php

get_header();

$url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$path = parse_url($url, PHP_URL_PATH);
$pathFragments = array_filter(explode('/', $path));
$end = end($pathFragments);
$theTown = str_replace('-', ' ', $end);

?>

<!-- Content -->
    <div class="container contents" style="margin-top: 9.5em;">
        <div class="row">
        	<div class="span12">

                <!-- Main Content -->
                <div class="main">

                	<section class="property-items">

                        <div class="narrative"></div>

                        <div class="property-items-container clearfix">

							<?php

								global $wpdb;
								$the_search_query = "select a.ID from blu_posts a, blu_properties b where b.property_id = a.post_excerpt and b.town like '%$theTown%' order by b.price asc";
								$the_search_query_results = $wpdb->get_results($the_search_query);

								$ir = array();
								foreach ($the_search_query_results as $val) {
								    $ir[] = $val->ID;
								}

								$new_args = array(
								    'post_type' => 'property',
								    'post__in' => $ir,
								    'posts_per_page' => 10,
								    'paged' => $paged
								);

								$nwq = new WP_Query( $new_args );

								if ( $nwq->have_posts() ) :
								    $post_count = 0;
								    while ( $nwq->have_posts() ) :
								        $nwq->the_post();

								        /* Display Property for Search Page */
								        get_template_part('template-parts/property-for-home');

								        $post_count++;
								        if(0 == ($post_count % 2)){
								            echo '<div class="clearfix"></div>';
								        }
								    endwhile;
								    wp_reset_query();
								else:
								    ?><div class="alert-wrapper"><h4><?php _e('No Properties Found!', 'framework') ?></h4></div><?php
								endif;

								theme_pagination( $nwq->max_num_pages);

							?>
						</div>
					</section>
				</div>
			</div>
		</div>
	</div>
<?php

get_footer();