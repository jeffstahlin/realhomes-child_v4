<?php
/*
*   Template Name: Property Search Template
*/
get_header();


/* Theme Home Page Module */
$theme_search_module = get_option('theme_search_module');

switch($theme_search_module){
    case 'properties-map':
        get_template_part('banners/map_based_banner');
        break;

    default:
        get_template_part('banners/default_page_banner');
        break;
}


?>

    <!-- Content -->
    <div class="container contents">
        <div class="row">

            <div class="span12">

                <!-- Main Content -->
                <div class="main">
                    <?php
                    /* Advance Search Form */
                    get_template_part('template-parts/advance-search');
                    ?>

                    <section class="property-items">

                        <div class="narrative"></div>

                        <div class="property-items-container clearfix">
                            <?php
                            /* List of Properties on Homepage */
                            $number_of_properties = intval(get_option('theme_properties_on_home'));
                            if(!$number_of_properties){
                                $number_of_properties = 4;
                            }

                            // Apply Search Filter
                            $search_args = apply_filters('real_homes_search_parameters');

                            $psav = prop_search_array_values($search_args);

                            global $wpdb;
                            $the_search_query = "select a.ID from blu_posts a, blu_properties b where b.property_id = a.post_excerpt and $psav order by b.price asc";
                            $the_search_query_results = $wpdb->get_results($the_search_query);

                            $ir = array();
                            foreach ($the_search_query_results as $val) {
                                $ir[] = $val->ID;
                            }

                            $new_args = array(
                                'post_type' => 'property',
                                'post__in' => $ir,
                                'posts_per_page' => $number_of_properties,
                                'paged' => $paged
                            );

                            $nwq = new WP_Query( $new_args );

                            if ( $nwq->have_posts() ) :
                                $post_count = 0;
                                while ( $nwq->have_posts() ) :
                                    $nwq->the_post();

                                    /* Display Property for Search Page */
                                    get_template_part('template-parts/property-for-home');

                                    $post_count++;
                                    if(0 == ($post_count % 2)){
                                        echo '<div class="clearfix"></div>';
                                    }
                                endwhile;
                                wp_reset_query();
                            else:
                                ?><div class="alert-wrapper"><h4><?php _e('No Properties Found!', 'framework') ?></h4></div><?php
                            endif;

                            ?>
                        </div>

                        <?php theme_pagination( $nwq->max_num_pages); ?>

                    </section>

                </div><!-- End Main Content -->

            </div> <!-- End span12 -->

        </div><!-- End  row -->

    </div><!-- End content -->

<?php get_footer(); ?>