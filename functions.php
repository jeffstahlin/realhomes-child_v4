<?php

/*-----------------------------------------------------------------------------------*/
/*  Load Required JS and CSS Styles
/*-----------------------------------------------------------------------------------*/
function load_site_scripts() {
    if ( !is_admin() ) {

        wp_register_style( 'common-css', get_stylesheet_directory_uri() . '/css/common.css' );
        wp_enqueue_style( 'common-css' );        

        wp_register_style( 'slidebars-css', get_stylesheet_directory_uri() . '/css/slidebars.min.css' );
        wp_enqueue_style( 'slidebars-css' );

        wp_deregister_style( 'awesome-font-css' );
        wp_dequeue_style( 'awesome-font-css' );

        wp_register_style( 'fontawesome-css', '//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css' );
        wp_enqueue_style( 'fontawesome-css' );

        if ( is_mobile() ) {
            wp_register_style( 'mobile-css', get_stylesheet_directory_uri() . '/css/mobile.css' );
            wp_enqueue_style( 'mobile-css' );
        }

        //wp_register_script('jquery-ui', 'https://code.jquery.com/ui/1.10.4/jquery-ui.min.js', array('jquery'), '1.10.4', false);
        //wp_enqueue_script('jquery-ui');

        if ( is_mobile() ) {
            /* wp_register_script('touchswipe-js', get_stylesheet_directory_uri() . '/js/jquery.touchSwipe.min.js', array('jquery'), '1.6', false);
            wp_enqueue_script('touchswipe-js'); */

            wp_register_script('iscroll-js', get_stylesheet_directory_uri() . '/js/iscroll.js', array('jquery'), '5.1.1', false);
            wp_enqueue_script('iscroll-js');
        }

        wp_register_script('allsite-js', get_stylesheet_directory_uri() . '/js/allsite.js', array('jquery'), '0.6', false);
        wp_enqueue_script('allsite-js');

        if ( !is_mobile() ) {
            wp_register_script('sticky-nav-js', get_stylesheet_directory_uri() .'/js/stickyNav.js', array('jquery'), '1.0', false);
            wp_enqueue_script('sticky-nav-js');
        } else if ( is_mobile() && is_front_page() ) {
            wp_register_script('sticky-nav-js', get_stylesheet_directory_uri() .'/js/stickyNav.js', array('jquery'), '1.0', false);
            wp_enqueue_script('sticky-nav-js');
        }

        // wp_register_script('gsap-js', 'http://cdnjs.cloudflare.com/ajax/libs/gsap/1.13.2/TweenMax.min.js', array('jquery'), '1.13.2', false);
        // wp_enqueue_script('gsap-js');

        // wp_register_script('jquery-gsap-js', 'http://cdnjs.cloudflare.com/ajax/libs/gsap/latest/utils/jquery.gsap.min.js', array('jquery'), '1.13.2', false);
        // wp_enqueue_script('jquery-gsap-js');

        wp_register_script('prefixfree-js', get_stylesheet_directory_uri() . '/js/prefixfree.min.js', array('jquery'), '1.0.7', false);
        wp_enqueue_script('prefixfree-js');

        wp_register_script('slidebars-js', get_stylesheet_directory_uri() . '/js/slidebars.js', array('jquery'), '0.9.4', false);
        wp_enqueue_script('slidebars-js');

        if( !is_singular('property') && !is_front_page() ) {
            wp_register_script('all-but-property-js', get_stylesheet_directory_uri() . '/js/all-but-property.js', array('jquery'), '0.6', false);
            wp_enqueue_script('all-but-property-js');
        }

    }
}
add_action('wp_enqueue_scripts', 'load_site_scripts');

function property_homepage_scripts() {
    if ( is_front_page() ) {

        wp_register_style( 'range-slider-css', get_stylesheet_directory_uri() . '/css/jquery.nouislider.min.css' );
        wp_enqueue_style( 'range-slider-css' );

        wp_register_style( 'home-parallax-css', get_stylesheet_directory_uri() . '/css/home-parallax.css' );
        wp_enqueue_style( 'home-parallax-css' );

        if ( is_ios() ) {
            wp_register_style( 'home-ios-css', get_stylesheet_directory_uri() . '/css/home-ios.css' );
            wp_enqueue_style( 'home-ios-css' );
        } else if ( is_android() ) {
            wp_register_style( 'home-android-css', get_stylesheet_directory_uri() . '/css/home-android.css' );
            wp_enqueue_style( 'home-android-css' );
        }

        wp_register_script('range-slider-js', get_stylesheet_directory_uri() . '/js/jquery.nouislider.min.js', array('jquery'), '6.1.0', false);
        wp_enqueue_script('range-slider-js');

        wp_register_script('range-slider-controls-js', get_stylesheet_directory_uri() . '/js/price_slider.js', array('jquery'), '1.0', false);
        wp_enqueue_script('range-slider-controls-js');

        wp_register_script('autocomplete-search-js', get_stylesheet_directory_uri() . '/search-autocomplete/jqueryui_autocomplete.js', array('jquery'), '1.0', false);
        wp_enqueue_script('autocomplete-search-js');

        wp_register_script('homepage-js', get_stylesheet_directory_uri() . '/js/homepage.js', array('jquery'), '1.0', false);
        wp_enqueue_script('homepage-js');

        /* if ( !is_mobile() ) {

            wp_register_script('parallax-js', get_stylesheet_directory_uri() .'/js/skrollr.min.js', array('jquery'), '0.6.24', false);
            wp_enqueue_script('parallax-js');

            wp_register_script('parallax-loader-js', get_stylesheet_directory_uri() .'/js/skrollr_main.js', array('jquery'), '1.0', false);
            wp_enqueue_script('parallax-loader-js');
        } */
    }
}
add_action('wp_enqueue_scripts', 'property_homepage_scripts');

function profile_js() {
    if ( is_page('profile') ) {
        wp_register_script('profile-js', get_stylesheet_directory_uri() . '/js/profile.js', array('jquery'), '1.0', false);
        wp_enqueue_script('profile-js');
    }
}
add_action('wp_enqueue_scripts', 'profile_js');

function property_search_css() {
    if ( is_page('property-listings') ) {
        wp_register_style( 'property-listings-css', get_stylesheet_directory_uri() . '/css/property-listings.css' );
        wp_enqueue_style( 'property-listings-css' );

        wp_register_script('property-pagination', get_stylesheet_directory_uri() . '/js/pagination-property-listings.js', array('jquery'), '1.0', false);
        wp_enqueue_script('property-pagination');
    }
}
add_action('wp_enqueue_scripts', 'property_search_css');

function custom_parallax_files() {
    if ( is_singular( 'property' ) ) {

        wp_register_style( 'parallax-css', get_stylesheet_directory_uri() . '/css/parallax.css' );
        wp_enqueue_style( 'parallax-css' );

        wp_register_style( 'animate-css', get_stylesheet_directory_uri() . '/css/animate.css' );
        wp_enqueue_style( 'animate-css' );

        wp_register_style( 'lightslider-css', get_stylesheet_directory_uri() . '/bower_components/lightslider/lightSlider/css/lightSlider.css' );
        wp_enqueue_style( 'lightslider-css' );

        wp_register_style( 'bootstrap-css', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css' );
        wp_enqueue_style( 'bootstrap-css' );

        //wp_register_style( 'rondell-css', get_stylesheet_directory_uri() . '/css/jquery.rondell.min.css' );
        //wp_enqueue_style( 'rondell-css' );

        //wp_register_script('rondell-js', get_stylesheet_directory_uri() .'/js/jquery.rondell.min.js', array('jquery'), '1.0', false);
        //wp_enqueue_script('rondell-js');

        // wp_register_script('justified-gallery-js', get_stylesheet_directory_uri() .'/js/jquery.justifiedGallery.min.js', array('jquery'), '1.0', false);
        // wp_enqueue_script('justified-gallery-js');

        // wp_register_script('panelSnap-js', get_stylesheet_directory_uri() .'/js/jquery.panelSnap.js', array('jquery'), '0.12.0', false);
        // wp_enqueue_script('panelSnap-js');

        wp_register_script('lightslider-js', get_stylesheet_directory_uri() . '/bower_components/lightslider/lightSlider/js/jquery.lightSlider.min.js', array('jquery'), '1.0', false);
        wp_enqueue_script('lightslider-js');

        wp_register_script('panelSnap-custom-js', get_stylesheet_directory_uri() .'/js/panelSnap.custom.js', array('jquery'), '1.0', false);
        wp_enqueue_script('panelSnap-custom-js');

        wp_register_script('pagination-js', get_stylesheet_directory_uri() .'/js/jquery.pajinate.min.js', array('jquery'), '1.0', false);
        wp_enqueue_script('pagination-js');

        if ( !is_mobile() ) {
            wp_register_script('sticky-nav-property-js', get_stylesheet_directory_uri() .'/js/stickyNav-property.js', array('jquery'), '1.0', false);
            wp_enqueue_script('sticky-nav-property-js');
        }

    }
}
add_action('wp_enqueue_scripts', 'custom_parallax_files');

function area_map_files() {
    if ( is_page('area-map') ) {
        wp_register_script( 'area-map-js', get_stylesheet_directory_uri() . '/js/area-map.js' );
        wp_enqueue_script( 'area-map-js' );
    }
}
add_action('wp_enqueue_scripts', 'area_map_files');

function sub_pages_css() {
    if ( !is_front_page() && !is_singular('property') ) {
        wp_register_style( 'all-pages-css', get_stylesheet_directory_uri() . '/css/all_pages.css' );
        wp_enqueue_style( 'all-pages-css' );
    }
}
add_action('wp_enqueue_scripts', 'sub_pages_css');

function search_properties_css() {
    if ( is_page('search-properties') ) {
        wp_register_style( 'search-properties-css', get_stylesheet_directory_uri() . '/css/search-properties.css' );
        wp_enqueue_style( 'search-properties-css' );
    }
}
add_action('wp_enqueue_scripts', 'search_properties_css');

function seoUrl($string) {
    //Lower case everything
    $string = strtolower($string);
    //Make alphanumeric (removes all other characters)
    $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
    //Clean up multiple dashes or whitespaces
    $string = preg_replace("/[\s-]+/", " ", $string);
    //Convert whitespaces and underscore to dash
    $string = preg_replace("/[\s_]/", "-", $string);
    return $string;
}


function load_custom_wp_admin_style() {
    wp_register_style( 'custom_wp_admin_css', get_stylesheet_directory_uri() . '/css/admin-style.css', false, '1.0.0' );
    wp_enqueue_style( 'custom_wp_admin_css' );
}
add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );


/****************************************************************/
/******************** Custom Search Form ************************/
/****************************************************************/
if ( is_front_page() ) {
    function property_search_custom($query) {
        if ( $query->is_search() ) {
            $query->set('post_type', 'property');

            $meta_query = array();

            // between min- and max-price search
            if( isset( $_GET['min-price'] ) && isset( $_GET['max-price'] ) ) {
                $min_price = doubleval($_GET['min-price']);
                $max_price = doubleval($_GET['max-price']);
                if( $min_price >= 0  && $max_price > $min_price ) {
                    $meta_query[] = array(
                        'key' => 'REAL_HOMES_property_price',
                        'value' => array( $min_price, $max_price  ),
                        'compare' => 'BETWEEN',
                        'type' => 'NUMERIC'
                    );
                }
            }

            $query->set('meta_query', $meta_query);

            return $query;
        }
    }
    add_action('pre_get_posts', 'property_search_custom', 1000);
}

/****************************************************************/
/********** Use Jetpacks's Photon without Jetpack ***************/
/****************************************************************/
add_filter( 'the_content', 'wpse39167_replace_image',9999 );
function wpse39167_replace_image( $content )
{
    $content = preg_replace_callback( "@<img.+?src=[\"'](.+?)[\"'].+?>@", 'wpse39167_maybe_replace_image', $content );
    return $content;
}

function wpse39167_maybe_replace_image($matches){
    if(!$matches[1])
        return $matches[0];

    $counter = wpse39167_static_counter( $matches[1] );
    $wp = 'http://i'.$counter.'.wp.com/';
    $url = str_replace(array('http://','https://'),$wp,$matches[1]);
    return str_replace($matches[1],$url,$matches[0]);
}

function wpse39167_static_counter( $url ) {
    srand( crc32( basename( $url ) ) );
    $static_counter = rand( 0, 2 );
    srand(); // this resets everything that relies on this, like array_rand() and shuffle()
    return $static_counter;
}

/****************************************************************/
/************ Real Homes Property Gallery Images ****************/
/****************************************************************/
/*
function rh_images_2() {
    $args = array( 'posts_per_page' => -1, 'post_type'=> 'property' );

    $myposts = get_posts( $args );

    foreach ( $myposts as $post ) : setup_postdata( $post );

        global $wpdb;
        $thePost = $post->ID;

        $query = "select ID, post_parent, post_type from $wpdb->posts
                     where post_parent = $thePost
                     and post_type like '%attachment%'
                     and ID not in (select meta_value from $wpdb->postmeta where post_id = $thePost)";
        $results = $wpdb->get_results($query);

        foreach( $results as $result ) {

            global $wpdb;

            $meta_key = 'REAL_HOMES_property_images';

            $wpdb->insert($wpdb->postmeta, array(
                'post_id' => $result->post_parent,
                'meta_key' => $meta_key,
                'meta_value' => $result->ID
            ));

        }
    endforeach;

    wp_reset_postdata();
}
*/

/****************************************************************/
/********** Blue Coast Property Importer Functions **************/
/****************************************************************/
function get_property_data() {
    global $wpdb;
    global $post;

    $id = get_the_ID();
    $the_post = get_post($id);
    $sql = "select property_id, price, type, status, description, images, surface_area_built, beds, baths, has_garage, categories, town, province, country from blu_properties where property_id = '$the_post->post_excerpt'";
    $the_query = $wpdb->get_results($sql);

    return $the_query;
}

function show_property_data($the_data) {

    $prop_data = get_property_data();
    $some_data = $prop_data;

    switch ($the_data):

        case 'price':

            foreach($some_data as $q) {

                $price_digits = doubleval($q->price);

                if($price_digits){
                    $currency = get_theme_currency();
                    $price_post_fix = $q->price;
                    $decimals = intval(get_option( 'theme_decimals'));
                    $decimal_point = get_option( 'theme_dec_point' );
                    $thousands_separator = get_option( 'theme_thousands_sep' );
                    $currency_position = get_option( 'theme_currency_position' );
                    $formatted_price = number_format($price_digits,$decimals, $decimal_point, $thousands_separator);
                    if($currency_position == 'after'){
                        return $formatted_price . $currency;
                    }else{
                        return $currency . $formatted_price;
                    }
                }
            }
            break;

        case 'city':

            foreach ($some_data as $cq) {
                echo $cq->town;
            }
            break;

        case 'metas':

            foreach($some_data as $mq) {
                $surface_area_built = $mq->surface_area_built;
                $beds = $mq->beds;
                $baths = $mq->baths;
                $garage = $mq->has_garage;

                return array($surface_area_built, $beds, $baths, $garage);
            }
            break;

        case 'images':

            foreach($some_data as $iq) {
                $imgs = $iq->images;
                $imgs = json_decode($imgs, true);
            }

            $images = $imgs['image'];

            echo '<div id="property-detail-flexslider" class="clearfix">';
            echo '<div class="flexslider">';
            echo '<ul class="slides">';

            foreach( $images as $the_image ){

                echo '<li data-thumb="'.$the_image['url'].'" data-src="'.$the_image['url'].'">';
                echo '<a href="'.$the_image['url'].'" class="'.get_lightbox_plugin_class() .'" '.generate_gallery_attribute().'>';
                echo '<img src="'.$the_image['url'].'" alt="'.$the_image['url'].'" />';
                echo '</a>';
                echo '</li>';
            }

            echo '</ul>';
            echo '</div>';
            echo '</div>';

            break;

        case 'gallery-images':

            foreach($some_data as $gi) {
                $imgs = $gi->images;
                $imgs = json_decode($imgs, true);
            }

            $images = $imgs['image'];

            return $images;
            break;

        case 'features':

            foreach($some_data as $fq) {
                $features = $fq->categories;
                $features = json_decode($features, true);
            }

            $feats = $features['category'];

            return $feats;
            break;

        default:
            foreach ($some_data as $sd) {

                if(!empty($sd->$the_data)) {
                    return $sd->$the_data;
                } else {
                    echo 'No table column matches this parameter';
                }

            }
            break;

    endswitch;
}

function get_search_form_data($data) {
    global $wpdb;
    global $post;

    $id = get_the_ID();
    $the_post = get_post($id);
    $sql = "select distinct $data from blu_properties order by $data asc";
    $the_query = $wpdb->get_results($sql);

    return $the_query;
}

function search_form_data($the_data) {

    $prop_data = get_search_form_data($the_data);
    $search_data = $prop_data;

    switch ($the_data):

        case 'town':

            foreach ($search_data as $cq) {
                $query_var = $_GET['location'];
                $location = $cq->town;
                if($location != $query_var) {
                    echo '<option value="' . $cq->town . '">' . $cq->town . '</option>';
                } else {
                    echo '<option value="' . $cq->town . '" selected="selected">' . $cq->town . '</option>';
                }
            }
            break;

        case 'status':

            foreach ($search_data as $stq) {
                $query_var = $_GET['status'];
                $status = $stq->status;
                if($status != $query_var) {
                    echo '<option value="' . $stq->status . '">' . $stq->status . '</option>';
                } else {
                    echo '<option value="' . $stq->status . '" selected="selected">' . $stq->status . '</option>';
                }

            }
            break;

        case 'type':

            foreach ($search_data as $tq) {
                $query_var = $_GET['type'];
                $type = $tq->type;
                if($type != $query_var) {
                    echo '<option value="' . $tq->type . '">' . $tq->type . '</option>';
                } else {
                    echo '<option value="' . $tq->type . '" selected="selected">' . $tq->type . '</option>';
                }

            }
            break;

        default:
            foreach ($search_data as $sd) {
                echo '<option value="' . $tq->type . '">' . $tq->type . '</option>';
            }
            break;

    endswitch;
}

// Get random property background
function get_property_bgs() {
    $i = 0;

    if($handle = opendir('/var/www/realestate/htdocs/wp-content/uploads/genericImages/')) {

        $data[] = array();

        while ((false !== ($entry = readdir($handle))) && ($i < 3)) {

            if ($entry != '.' && $entry != '..') {

                $data[] = $entry;
            }

            $i++;

        }

        return $data;

        closedir($handle);
    }
}

/* Add Custom Columns */
if( !function_exists( 'property_edit_columns' ) ){
    function property_edit_columns($columns)
    {

        $columns = array(
            "cb" => "<input type=\"checkbox\" />",
            "id" => __( 'ID', 'framework' ),
            "title" => __( 'Property Title','framework' ),
            "thumb" => __( 'Thumbnail','framework' ),
            "city" => __( 'City','framework' ),
            "type" => __('Type','framework'),
            "status" => __('Status','framework'),
            "price" => __('Price','framework'),
            "bed" => __('Beds','framework'),
            "bath" => __('Baths','framework'),
            "garage" => __('Garages','framework'),
            "date" => __( 'Publish Time','framework' )
        );

        return $columns;
    }
}
add_filter("manage_edit-property_columns", "property_edit_columns");

// Override the default value calls for the property posts in the admin
function property_custom_columns($column){
    global $post;
    switch ($column)
    {
        case 'id':
            echo 'BC' . $post->ID;
            break;
        case 'thumb':
            $the_images = show_property_data('gallery-images');
            if( !empty($the_images) ){
                ?>
                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                    <div class="home-property-image" style="background: url(<?php echo $the_images[0]['url']; ?>); width: 244px; height: 180px; display: block; background-size: 100%; background-repeat: no-repeat;">

                    </div>
                </a>
                <?php
            }
            else{
                _e('No Thumbnail','framework');
            }
            break;
        case 'city':
            echo show_property_data('city');
            break;
        case 'type':
            echo show_property_data('type');
            break;
        case 'status':
            echo show_property_data('status');
            break;
        case 'price':
            echo show_property_data('price');
            break;
        case 'bed':
            $the_metas = show_property_data('metas');
            $bed = $the_metas[1];
            if(!empty($bed)){
                echo $bed;
            }
            else{
                _e('NA','framework');
            }
            break;
        case 'bath':
            $the_metas = show_property_data('metas');
            $bath = $the_metas[2];
            if(!empty($bath)){
                echo $bath;
            }
            else{
                _e('NA','framework');
            }
            break;
        case 'garage':
            $the_metas = show_property_data('metas');
            $garage = $the_metas[3];
            if(!empty($garage)){
                echo $garage;
            }
            else{
                _e('NA','framework');
            }
            break;
    }
}
add_action("manage_posts_custom_column", "property_custom_columns");

// Sortable columns
/*
function property_id_register_sortable( $columns )
{
    $columns['id'] = 'id';
    return $columns;
}

add_filter("manage_edit-property_sortable_columns", "property_id_register_sortable" );
/*

/*-----------------------------------------------------------------------------------*/
/*  Properties Search Filter
/*-----------------------------------------------------------------------------------*/
function real_homes_search($search_args){

    /* taxonomy query and meta query arrays */
    $prop_query = array();

    /* Property ID Parameter */
    if( isset($_GET['property-id']) && !empty($_GET['property-id']) && ($_GET['property-id'] != 'Any') ){
        $property_id = trim($_GET['property-id']);
        array_push($prop_query, $property_id);
    }

    /* property city(location) taxonomy query */
    if( (!empty($_GET['location'])) && ( $_GET['location'] != 'any') ){
        $loc_array = array('town' => $_GET['location']);
        array_push($prop_query, $loc_array);
    }

    /* property type taxonomy query */
    if( (!empty($_GET['type'])) && ( $_GET['type'] != 'any') ){
        $typ_array = array('type' => $_GET['type']);
        array_push($prop_query, $typ_array);
    }

    /* property status taxonomy query */
    if((!empty($_GET['status'])) && ( $_GET['status'] != 'any' ) ){
        $sta_array = array('status' => $_GET['status']);
        array_push($prop_query, $sta_array);
    }

    /* Property Bedrooms Parameter */
    if((!empty($_GET['bedrooms'])) && ( $_GET['bedrooms'] != 'any' ) ){
        $bed_array = array('beds' => $_GET['bedrooms']);
        array_push($prop_query, $bed_array);
    }

    /* Property Bathrooms Parameter */
    if((!empty($_GET['bathrooms'])) && ( $_GET['bathrooms'] != 'any' ) ){
        $bat_array = array('baths' => $_GET['bathrooms']);
        array_push($prop_query, $bat_array);
    }

    /* Logic for Min and Max Price Parameters */
    if( isset($_GET['min-price']) && ($_GET['min-price'] != 'any') && isset($_GET['max-price']) && ($_GET['max-price'] != 'any') ){
        $min_price = doubleval($_GET['min-price']);
        $max_price = doubleval($_GET['max-price']);
        $min_array = array('min_price' => $min_price);
        $max_array = array('max_price' => $max_price);
        array_push($prop_query, $min_array);
        array_push($prop_query, $max_array);
    }elseif( isset($_GET['min-price']) && ($_GET['min-price'] != 'any') ){
        $min_price = doubleval($_GET['min-price']);
        $min_array = array('min_price' => $min_price);
        if( $min_price > 0 ){
            array_push($prop_query, $min_array);
        }
    }elseif( isset($_GET['max-price']) && ($_GET['max-price'] != 'any') ){
        $max_price = doubleval($_GET['max-price']);
        $max_array = array('max_price' => $max_price);
        if( $max_price > 0 ){
            array_push($prop_query, $max_array);
        }
    }

    /* Logic for Min and Max Area Parameters */
    if( isset($_GET['min-area']) && !empty($_GET['min-area']) && isset($_GET['max-area']) && !empty($_GET['max-area']) ){
        $min_area = intval($_GET['min-area']);
        $max_area = intval($_GET['max-area']);
        if( $min_area >= 0 && $max_area > $min_area ){
            $min_area_array = array('min_area' => $min_area);
            $max_area_array = array('max_area' => $max_area);
            array_push($prop_query, $min_area_array);
            array_push($prop_query, $max_area_array);
        }
    }elseif( isset($_GET['min-area']) && !empty($_GET['min-area']) ){
        $min_area = intval($_GET['min-area']);
        if( $min_area > 0 ){
            $min_area_array = array('min_area' => $min_area);
            array_push($prop_query, $min_area_array);
        }
    }elseif( isset($_GET['max-area']) && !empty($_GET['max-area']) ){
        $max_area = intval($_GET['max-area']);
        if( $max_area > 0 ){
            $max_area_array = array('max_area' => $max_area);
            array_push($prop_query, $max_area_array);
        }
    }


    return $prop_query;
}
add_filter('real_homes_search_parameters','real_homes_search');

// For Advanced Search
function prop_search_array($the_args) {
    $ret = '';
    foreach ($the_args as $i => $loc) {
        $loc_key = key($loc);

        // Set the commas to separate the values
        if ($i > 0 && $loc_key != 'max_price') {
            $ret .= ',';
        }

        // Logic to make min and max price be labeled as just price
        if ($loc_key == 'min_price') {
            $loc_key = 'price';
        } elseif ($loc_key == 'max_price') {
            $loc_key = '';
        }

        // Logic to make min and max area be labeled as surface area built
        if ($loc_key == 'min_area') {
            $loc_key = 'surface_area_built';
        } elseif ($loc_key == 'max_area') {
            $loc_key = '';
        }

        $ret .= ' ' . $loc_key;
    }
    return $ret;
}

function prop_search_array_values($other_args) {
    $val = '';
    foreach ($other_args as $i => $oa) {

        $the_key = key($oa);

        if (($i > 0)) {
            $val .= ' and';
        }

        if ($the_key == 'min_price') {

            $val .= " b.price between " . $oa[$the_key];

        } elseif ($the_key == 'max_price') {

            $val .= " " . $oa[$the_key];

        } elseif ($the_key == 'beds' || $the_key == 'baths') {

            $val .= " b." . $the_key . " >= " . $oa[$the_key];

        } elseif ($the_key == 'min_area') {

            $val .= " b.surface_area_built between " . $oa[$the_key];

        } elseif ($the_key == 'max_area') {

            $val .= " " . $oa[$the_key];

        } else {

            $val .= " b." . $the_key . " like '%" . $oa[$the_key] . "%'";

        }
    }
    return $val;
}

/*-----------------------------------------------------------------------------------*/
//  Widgets
/*-----------------------------------------------------------------------------------*/
    require_once( get_stylesheet_directory() . '/widgets/' . 'featured-properties-widget.php');
    require_once( get_stylesheet_directory() . '/widgets/' . 'property-types-widget.php');
    require_once( get_stylesheet_directory() . '/widgets/' . 'advance-search-widget.php');


/*-----------------------------------------------------------------------------------*/
//  Register Widgets
/*-----------------------------------------------------------------------------------*/
if( !function_exists( 'register_theme_widgets' ) ){
    function register_theme_widgets() {
        register_widget( 'Featured_Properties_Widget' );
        register_widget( 'Property_Types_Widget' );
        register_widget( 'Advance_Search_Widget' );
    }
}

add_action( 'widgets_init', 'register_theme_widgets' );


// Require the forceUTF8 encoder
require_once( get_stylesheet_directory() . '/ForceUTF8/Encoding.php');

// Search in admin allows search by post id
$js_searchbyid = new jsSearchById();

    class jsSearchById
    {
        function jsSearchById()
        {
            $this->__construct();
        }

        function __construct()
        {
            add_filter('posts_where', array(&$this, 'posts_where'));
        }

        function posts_where($where)
        {
            if(is_admin() && is_search())
            {
                $s = $_GET['s'];

                if(!empty($s))
                {
                    if(is_numeric($s))
                    {
                        global $wpdb;
                        $where .= ' or ' . $wpdb->posts . '.ID = ' . $s;
                    }
                    elseif(preg_match("/^(\d+)(,\s*\d+)*\$/", $s)) // string of post IDs
                    {
                        global $wpdb;
                        $where .= ' or ' . $wpdb->posts . '.ID in (' . $s . ')';
                    }
                }
            }

            return $where;
        }
    }


add_filter( 'widget_text', 'shortcode_unautop');
add_filter( 'widget_text', 'do_shortcode');

/****************************************************/
/************* Landing Pages Custom Post ************/
/****************************************************/

if ( ! function_exists('landing_pages') ) {

// Register Custom Post Type
function landing_pages() {

    $labels = array(
        'name'                => _x( 'Landing Pages', 'Post Type General Name', 'framework' ),
        'singular_name'       => _x( 'Landing Page', 'Post Type Singular Name', 'framework' ),
        'menu_name'           => __( 'Landing Pages', 'framework' ),
        'parent_item_colon'   => __( 'Parent Landing Page:', 'framework' ),
        'all_items'           => __( 'All Landing Pages', 'framework' ),
        'view_item'           => __( 'View Landing Page', 'framework' ),
        'add_new_item'        => __( 'Add New Landing Page', 'framework' ),
        'add_new'             => __( 'Add New', 'framework' ),
        'edit_item'           => __( 'Edit Landing Page', 'framework' ),
        'update_item'         => __( 'Update Landing Page', 'framework' ),
        'search_items'        => __( 'Search Landing Page', 'framework' ),
        'not_found'           => __( 'Not found', 'framework' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'framework' ),
    );
    $args = array(
        'label'               => __( 'landing_pages', 'framework' ),
        'description'         => __( 'Landing Pages for Properties', 'framework' ),
        'labels'              => $labels,
        'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail', 'trackbacks', 'revisions', 'custom-fields', 'page-attributes' ),
        'taxonomies'          => array( 'category', 'post_tag' ),
        'hierarchical'        => true,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
    );
    register_post_type( 'landing_pages', $args );

}

// Hook into the 'init' action
add_action( 'init', 'landing_pages', 0 );

}

// Turn price to float
function priceToFloat($s) {
    // convert "," to "."
    $s = str_replace(',', '.', $s);
    // remove all but numbers "."
    $s = preg_replace("/[^0-9\.]/", "", $s);

    // check for cents
    $hasCents = (substr($s, -3, 1) == '.');
    // remove all seperators
    $s = str_replace('.', '', $s);
    // insert cent seperator
    if ($hasCents)
    {
        $s = substr($s, 0, -2) . '.' . substr($s, -2);
    }
    // return float
    return (float) $s;
}

?>