<?php
    get_header( 'property' );

        // Start Header
        if ( !wp_is_mobile() ) { ?>
            <div id="pheader" class="header-wrapper" style="position: fixed; width: 100%; top: 0; z-index: 1000;">
            <?php
        } else { ?>
            <div id="pheader" class="header-wrapper shrink" style="position: fixed; width: 100%; top: 0; z-index: 1000;">
            <?php
        }
    ?>

            <div class="container"><!-- Start Header Container -->

                <header id="header" class="clearfix">

                    <div id="header-top" class="clearfix">
                        <?php
                        /* WPML Language Switcher */
                        if(function_exists('icl_get_languages')){
                            $wpml_lang_switcher = get_option('theme_wpml_lang_switcher');
                            if($wpml_lang_switcher == 'true'){
                                do_action('icl_language_selector');
                            }
                        }

                        $header_phone = get_option('theme_header_phone');
                        if( !empty($header_phone) ){
                            ?>
                            <h2 class="contact-number">
                                <div class="number">
                                    <div class="click-to-call"><?php _e('Click to call', 'framework') ?></div>
                                    <?php _e('or phone us on', 'framework') ?> <?php echo $header_phone ?>
                                </div>
                                <i class="fa fa-phone"></i>
                            </h2>
                            <?php
                        }

                        $header_email = get_option('theme_header_email');
                        if(!empty($header_email)){
                            ?>
                            <h2 id="contact-email">
                                <a href="mailto:<?php echo antispambot($header_email); ?>">
                                    <i class="fa fa-envelope"></i>
                                </a>
                            </h2>
                            <?php
                        }
                        ?>

                        <!-- Social Navigation -->
                        <?php  get_template_part('template-parts/social-nav') ;    ?>


                        <?php
                        $enable_user_nav = get_option('theme_enable_user_nav');
                        if( $enable_user_nav == "true" ){
                            ?>
                            <div class="user-nav clearfix">
                                <?php
                                if(is_user_logged_in()){

                                    $submit_url = get_option('theme_submit_url');
                                    $my_properties_url = get_option('theme_my_properties_url');

                                    if(!empty($submit_url)){
                                        ?><a href="<?php echo $submit_url; ?>"><i class="fa fa-plus-circle"></i><?php _e('Submit Property','framework'); ?></a><?php
                                    }

                                    if(!empty($my_properties_url)){
                                        ?><a href="<?php echo $my_properties_url; ?>"><i class="fa fa-th-list"></i><?php _e('My Properties','framework'); ?></a><?php
                                    }
                                    ?>
                                    <a href="<?php echo admin_url( 'profile.php' ); ?>"><i class="fa fa-user"></i><?php _e('Profile','framework'); ?></a>
                                    <a class="last" href="<?php echo wp_logout_url( home_url() ); ?>"><i class="fa fa-sign-out"></i><?php _e('Logout','framework'); ?></a>
                                    <?php

                                }else{
                                    $theme_login_url = get_option('theme_login_url');
                                    if(!empty($theme_login_url)){
                                        ?>
                                        <a href="<?php echo $theme_login_url; ?>"><i class="fa fa-sign-in"></i><?php _e('Login','framework'); ?></a>
                                        <a class="last" href="<?php echo $theme_login_url; ?>"><?php _e('Register','framework'); ?></a>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                            <?php
                        }
                        ?>

                    </div>

                    <!-- Logo -->
                    <?php
                        if ( !wp_is_mobile() || wp_is_mobile() && is_front_page() ) {
                    ?>
                        <div id="logo">

                            <?php
                            $logo_path = get_option('theme_sitelogo');
                            if(!empty($logo_path)){
                                ?>
                                <a title="<?php  bloginfo( 'name' ); ?>" href="<?php echo home_url(); ?>">
                                    <img src="<?php echo $logo_path; ?>" alt="<?php  bloginfo( 'name' ); ?>">
                                </a>
                                <h2 class="logo-heading only-for-print">
                                    <a href="<?php echo home_url(); ?>"  title="<?php bloginfo( 'name' ); ?>">
                                        <?php  bloginfo( 'name' ); ?>
                                    </a>
                                </h2>
                                <?php
                            }else{
                                ?>
                                <h2 class="logo-heading">
                                    <a href="<?php echo home_url(); ?>"  title="<?php bloginfo( 'name' ); ?>">
                                        <?php  bloginfo( 'name' ); ?>
                                    </a>
                                </h2>
                                <?php
                            }

                            ?>
                        </div>
                <?php } ?>
                </header>

                <!-- Start Main Menu-->
                <?php
                    if ( !wp_is_mobile() || wp_is_mobile() && is_front_page() ) { ?>
                        <div class="main-menu-holder">
                        <?php
                    } else { ?>
                        <div class="main-menu-holder sticky">
                        <?php
                    }
                ?>
                    <div>
                        <nav class="main-menu">
                            <div class="sticky-logo">
                                <a href="<?php echo home_url(); ?>"  title="<?php bloginfo( 'name' ); ?>">
                                    <img src="/wp-content/uploads/2014/04/sticky-logo.png">
                                </a>
                            </div>
                            <?php
                            wp_nav_menu( array(
                                'theme_location' => 'main-menu',
                                'menu_class' => 'clearfix'
                            ));
                            ?>
                            <div id="mobile-button">
                                <a id="responsive-menu-button" href="#mobile-menu">
                                    Menu <i class="fa fa-list-ul"></i>
                                </a>
                            </div>
                        </nav>
                    </div>
                </div>
                <!-- End Main Menu -->

            </div> <!-- End Header Container -->

        </div><!-- End Header -->

        <div id="sb-site">

            <main id="overview" class="single-property">

                <section id="slide-0" class="homeSlide" data-panel="first">

                    <div class="bcg"

                        <?php if ( is_handheld() ) { ?>

                            style="background-color: white"

                        <?php } else { ?>

                            style="background-image:url('<?php $gpb =  get_property_bgs(); $uploads_dir = wp_upload_dir(); echo $uploads_dir['baseurl'] . "/genericImages/" . $gpb[1]; ?>')"

                        <?php } ?>

                    >

                        <div class="hsContainer">
                            <div class="hsContent">
                                <div class="page-head">
                                    <div class="container">
                                        <div class="wrap clearfix container" style="margin-top: 0;">
                                            <h1 class="page-title" id="propId"><span><?php echo 'BC' . $post->ID; ?></span></h1>
                                            <div class="listing-header">
                                                <p>
                                                    <?php
                                                        /* Property Title */
                                                        $someid = get_the_ID();
                                                        $the_title = get_post($someid);
                                                        $f_title = preg_replace('/(.+?)_\d{1,9}$/', '$1', $the_title->post_title);
                                                        $utf8_f_title = \ForceUTF8\Encoding::fixUTF8($f_title);
                                                        echo $utf8_f_title;
                                                    ?>
                                                    <span class="pull-right" id="propertyPrice">
                                                        <?php echo show_property_data('price'); ?>
                                                    </span>
                                                </p>
                                                <div class="content">
                                                    <?php
                                                        if ( have_posts() ) :
                                                            while ( have_posts() ) :
                                                                the_post();
                                                                get_template_part('property-details/property-contents');

                                                            endwhile;
                                                        endif;
                                                    ?>
                                                    <?php echo show_property_data('images'); ?>
                                                </div>
                                            </div>
                                            <div class="property-meta clearfix">
                                                <?php
                                                    /* Property Icons */
                                                    get_template_part('property-details/property-metas');
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container">
                                        <div class="col-lg-6">
                                            <div class="the-amenities">
                                                <div class="features">
                                                    <h4 class="title"><?php _e('Features', 'framework'); ?></h4>
                                                    <ul class="arrow-bullet-list clearfix">
                                                        <?php
                                                        /* Property Features */
                                                        $property_features = show_property_data('features');
                                                        if(!empty($property_features)){
                                                            foreach ($property_features as $pf) {

                                                                if(!empty($pf['value']['uk'])) {

                                                                    echo '<li><strong>'.$pf['name']['uk'].'</strong>: ' . $pf['value']['uk'] . '</li>';

                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="the-features">
                                                <div class="type-container clearfix clear">
                                                    <h2>
                                                        <?php
                                                            echo '<span><i class="fa fa-building"></i> ' . show_property_data('type');
                                                        ?>
                                                    </h2>
                                                </div>
                                                <div class="price-container clearfix clear">
                                                    <h2>
                                                        <?php
                                                            echo '<span>' . show_property_data('price') . '</span>';
                                                        ?>
                                                    </h2>
                                                </div>
                                                <?php
                                                    $the_metas = show_property_data('metas');

                                                    if( !empty($the_metas[0]) ) { ?>

                                                        <div class="size-container clearfix clear">
                                                            <h2>
                                                                <?php
                                                                    $prop_size = $the_metas[0];
                                                                    echo '<span><i class="fa fa-home"></i> ';
                                                                    echo $prop_size . ' m<sup>2</sup>';
                                                                    if( !empty($post_meta_data['REAL_HOMES_property_size_postfix'][0]) ){
                                                                        $prop_size_postfix = $post_meta_data['REAL_HOMES_property_size_postfix'][0];
                                                                        echo '&nbsp;'.$prop_size_postfix;
                                                                    }
                                                                    echo '</span>';
                                                                ?>
                                                            </h2>
                                                        </div>
                                                <?php
                                                    }

                                                    //$post_meta_data = get_post_custom($post->ID);

                                                    if( !empty($the_metas[1]) ) { ?>

                                                        <div class="bedrooms-container clearfix clear">
                                                            <h2>
                                                                <?php
                                                                    $prop_bedrooms = floatval($the_metas[1]);
                                                                    $bedrooms_label = ($prop_bedrooms > 1)? __('Bedrooms','framework' ): __('Bedroom','framework');
                                                                    echo '<span><i class="icon-bedrooms"></i> '. $prop_bedrooms .'&nbsp;'.$bedrooms_label.'</span>';
                                                                ?>
                                                            </h2>
                                                        </div>
                                                <?php
                                                    }
                                                    if( !empty($the_metas[2]) ) { ?>

                                                        <div class="bathrooms-container clearfix clear">
                                                        <h2>
                                                            <?php
                                                                $prop_bathrooms = floatval($the_metas[2]);
                                                                $bathrooms_label = ($prop_bathrooms > 1)?__('Bathrooms','framework' ): __('Bathroom','framework');
                                                                echo '<span><i class="icon-bathrooms"></i> '. $prop_bathrooms .'&nbsp;'.$bathrooms_label.'</span>';
                                                            ?>
                                                        </h2>
                                                    </div>

                                                <?php
                                                    }
                                                ?>

                                            </div>
                                        </div>
                                    </div><!-- End Container -->
                                </div><!-- End Page Head -->
                            </div>
                        </div>
                    </div>
                </section>

                <!-- Content -->
                <section id="slide-4" class="contents detail homeSlide" data-slide="fourth">

                    <div class="row">
                        <div class="main-wrap full-width">

                            <!-- Main Content -->
                            <div class="main">

                                <section id="overview">

                                 <?php
                                 if ( have_posts() ) :
                                     while ( have_posts() ) :
                                        the_post();

                                        get_template_part('property-details/property-video');

                                        get_template_part('property-details/property-agent');

                                     endwhile;
                                 endif;
                                 ?>
                                </section>

                            </div><!-- End Main Content -->

                            <?php
                            /*
                             * 6. Similar Properties
                             */
                                get_template_part('property-details/similar-properties-property');
                            ?>

                        </div> <!-- End span9 -->
                    </div><!-- End contents row -->

                    <?php get_footer('property'); ?>
                </section>
            </main>
            <script type="text/javascript" src="http://www.skypeassets.com/i/scom/js/skype-uri.js"></script>