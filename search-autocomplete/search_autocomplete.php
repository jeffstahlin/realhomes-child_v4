<?php
require_once('/var/www/realestate/wp-config.php');
global $wpdb;
/* retrieve the search term that autocomplete sends */

$location_name = trim(strip_tags($_GET['term']));
$property_location_search = "select distinct town from blu_properties where town like '%$location_name%'";
$search_property_location = $wpdb->get_results($property_location_search, ARRAY_A);

$search_array = array();

foreach ($search_property_location as $spl) {
	$town = $spl['town'];
	array_push($search_array, $town);
}

// jQuery wants JSON data
echo json_encode($search_array);


flush();

?>