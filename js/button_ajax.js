function my_js_function() {

	jQuery('#wp-admin-bar-update-images a').html('<img src="/wp-content/themes/realhomes-child_v2/images/ajax-loader.gif" />');

	jQuery.ajax({

		url: my_ajax_script.ajaxurl,
		data: ({action : 'setRef'}),

		success: function() {
			jQuery('#wp-admin-bar-update-images a').attr('class', 'img-updated').html('Ref ID Set');
		},
		error: function() {
			jQuery('#wp-admin-bar-update-images a').attr('class', 'img-not-updated').html('Error Setting Ref ID');
		}
	});
}