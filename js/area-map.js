//Pagination - Ajaxify the Pagination
jQuery(function($) {
  $('.listing-layout').on('click', '.pagination a', function(e){

    e.preventDefault();
    var link = $(this).attr('href');

    $('.list-container').fadeOut(500, function(){
			//$('.loader').css('display', 'block');
      $(this).load(link + ' .list-container > .span6', function() {
        //$('.loader').css('display', 'none');
        $(this).fadeIn(500);
      });
    });

    $('.pagination').fadeOut(500, function(){
			$(this).load(link + ' .pagination > a', function(){
				$(this).fadeIn(500);
			});
    });

  });
});