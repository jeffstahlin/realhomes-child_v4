jQuery(document).ready(function($) {

    /* Panel Snap - Initiate plugin with settings
    $('.single-property').panelSnap({
        $menu: false, // jQuery object referencing a menu that contains menu items
        directionThreshold: 50, // An integer specifying the ammount of pixels required to scroll before the plugin detects a direction and snaps to the next panel.
        slideSpeed: 500 // The ammount of miliseconds in which the plugin snaps to the desired panel

    }); */

    // Call to action - Slide up/down and tabs
    $('.call-to-action .fa').click(function(event) {

      var theid = event.target.id;
      var popUp = $(event.target).closest('.call-to-action').prev('.pop-up');

      if ( popUp.hasClass('active') && popUp.find('.' + theid).hasClass('active') ) {

        popUp.removeClass('active');
        popUp.find('.' + theid).removeClass('active');

      } else if ( popUp.hasClass('active') && !popUp.find('.' + theid).hasClass('active') ) {

        popUp.find('.action-content').removeClass('active');
        popUp.find('.' + theid).addClass('active');

      } else if ( !popUp.hasClass('active') ) {

        popUp.addClass('active');
        popUp.find('.' + theid).addClass('active');

      }

      // Add class selected to focused call to action button to change color
      if ( !$(this).hasClass('selected') ) {

        var thisone = $(this);
        $(event.target).closest('.call-to-action').find('.fa').removeClass('selected');
        $(thisone).addClass('selected');

      } else {

        $(this).removeClass('selected');

      }

      // Change the height of the pop up if the bookmark button is clicked
      if ( $(this).hasClass('fa-bookmark') && popUp.find('.' + theid).hasClass('active') ) {

        if ( popUp.find('.bookmark').length > 3 ) {
          var bkmrkHeight = ( $('.bookmark').outerHeight() * 3 ) + 249;
          popUp.attr('style', 'height: ' + bkmrkHeight + 'px');
        } else {
          var bkmrkHeight = ( $('.bookmark').outerHeight() * popUp.find('.bookmark').length ) + 249;
          popUp.attr('style', 'height: ' + bkmrkHeight + 'px');
        }

      } else {
        var attr = popUp.attr('style');

        if (typeof attr !== typeof undefined && attr !== false) {
          popUp.removeAttr('style');
        }

      }

    });

    // Call to action - Add class to submit button
    if ( $('.call-to-action input[type="submit"]').hasClass('btn-success') !== true ) {
      $('.call-to-action input[type="submit"]').addClass('btn btn-success');
    }

    // Email call to action - Add placeholders to the input fields
    $(function() {
      $('.gfield').each(function() {
        var thename = $(this).children('.gfield_label').html();

        if ( thename === 'Enter message' ) {

          $(this).children('.gfield_label').siblings('.ginput_container').children('textarea').attr('placeholder', thename);

        } else {

          $(this).children('.gfield_label').siblings('.ginput_container').children('input').attr('placeholder', thename);
        }
      });
    });

    // Image Gallery - Make the first image in the gallery show as the big image
    $(function() {
      var firstImgUrl = $('#propertyGallery div:first-child img').attr('src');
      $('.the-image-container').html('<img src="' + firstImgUrl + '" />');
    });

    /* Image Gallery - Give masonry layout to the images
    $('#propertyGallery').justifiedGallery({
      sizeRangeSuffixes: {'lt100':'-244x163',
                          'lt240':'-244x163',
                          'lt320':'-300x225',
                          'lt500':'',
                          'lt640':'_z',
                          'lt1024':'_b'},
      maxRowHeight: 120
    }); */

    // Image Gallery - lightSlider
    $(function() {
      $('#propertyGallery').lightSlider({
          gallery:true,
          item:1,
          thumbItem:9,
          slideMargin:0,
          currentPagerPosition:'left',
          onSliderLoad: function(plugin) {
              plugin.lightGallery();
          }
      });
    });

    // Image Gallery - Change the image url of the big image to the one the user has clicked on
    $('#propertyGallery img').click(function() {
      $(this).prop('src', function (i, v) {
        var imgUrl =  v.replace(/-\d+x\d+\.jpeg$/, '.jpeg');
        $('.the-image-container img').fadeOut('slow', function() {
          $(this).attr('src', imgUrl);
          $(this).fadeIn('slow');
        });
      });
    });

});

// scroll handler
jQuery(document).ready(function($) {
    var scrollToAnchor = function( id ) {
        // grab the element to scroll to based on the name
        var elem = $("a[name='"+ id +"']");
        // if that didn't work, look for an element with our ID
        if ( typeof( elem.offset() ) === "undefined" ) {
          elem = $("#"+id);
        }
        // if the destination element exists
        if ( typeof( elem.offset() ) !== "undefined" ) {
          // do the scroll
          $('main.single-property').animate({
                  scrollTop: elem.offset().top
          }, 1000 );
        }
    };

    // bind to click event
    $("a").click(function( event ) {
        // only do this if it's an anchor link
        if ( $(this).attr("href").match("#") ) {
            var idHref = $(this).attr('id');
            // cancel default event propagation
            event.preventDefault();
            // scroll to the location
            var href = $(this).attr('href').replace('#', idHref);
            scrollToAnchor( href );
        }
    });
});

//Bookmark functionality on the property pages
jQuery(document).ready(function($) {

  if (localStorage.getItem('properties') !== null ) {
    var a = localStorage.getItem('properties');
    var jsonData = JSON.parse(a);

    $.each(jsonData, function(key, value) {
      $('.bookmarks').append('<li class="bookmark" id="' + value.id + '"><span class="remove-prop">x</span><a href="' + value.location + '" class="apb"><img src="' + value.image + '" alt="' + value.title + '"><p>' + value.title + '</p><div class="property_ref">Ref: ' + value.id + '</div><div class="property_price">Price: ' + value.price + '</div><span class="clearfix"></span></a></li>');
    });

  }

  $('#addBookmark').click(function() {

    var pageURL = window.location.pathname;
    var pageTitle = $("#galleryScroll").attr('title');
    var favImage = $("#mainImg").attr('src');
    var propId = document.getElementById("propId").innerText || document.getElementById("propId").textContent;
    var propPrice = document.getElementById("propertyPrice").innerText || document.getElementById("propertyPrice").textContent;

    var test = localStorage.getItem('properties');
    var obj = [];
    var exists = null;

    if ( test ) {
      obj = JSON.parse(test);

      for (i=0;i<obj.length;i++) {
        if (obj[i].id === propId) {
          exists = true;
        }
      }
    }

    if ( ( obj.length === 0 ) || ( exists !== true ) ) {
      obj.push({ "location": pageURL, "title": pageTitle, "image": favImage, "id": propId, "price": propPrice });
      localStorage.setItem('properties',JSON.stringify(obj));

      $('.bookmarks').append('<li class="bookmark" id="' + propId + '"><span class="remove-prop">x</span><a href="' + pageURL + '" class="apb"><img src="' + favImage + '" alt="' + pageTitle + '"><p>' + pageTitle + '</p><div class="property_ref">Ref: ' + propId + '</div><div class="property_price">Price: ' + propPrice + '</div><span class="clearfix"></span></a></li>');
    } else {
      alert('already exists');
    }

    console.log(localStorage.getItem('properties'));
  });

  $('.clear-all').click(function() {
    localStorage.removeItem('properties');

    $('.bookmarks').children().remove();
  });

  $('.remove-prop').click(function() {
    var json = JSON.parse(localStorage.getItem("properties"));
    var item = $(this).parent().attr('id');

    alert(item);

    for (i=0;i<json.length;i++) {
      if (json[i].id == item) {
        json.splice(i,1);
      }
    }
    localStorage["properties"] = JSON.stringify(json);

    $(this).parent().remove();
  });

  $('#bookmarks_container').pajinate({
    items_per_page : 3,
    item_container_id : '#bookmarks',
    nav_panel_id : '.page_navigation',
    nav_label_first : '<<',
    nav_label_last : '>>',
    nav_label_prev : '<',
    nav_label_next : '>',
    abort_on_small_lists: true
  });

});
