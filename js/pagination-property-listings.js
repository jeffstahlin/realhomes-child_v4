//Pagination - Ajaxify the Pagination on Property Listings page
jQuery(function($) {
  $('.listing-layout').on('click', '.pagination a', function(e){

    e.preventDefault();
    var link = $(this).attr('href');

    $('.list-container').fadeOut(500, function(){
			$('.loader').css('display', 'block');
      $(this).load(link + ' .list-container > *', function() {
        $('.loader').css('display', 'none');
        $(this).fadeIn(500);
      });
    });

    $('.pagination').fadeOut(500, function(){
			$(this).load(link + ' .pagination > a', function(){
				$(this).fadeIn(500);
			});
    });

  });
});

// Pagination - Keep the height of the elements while ajax loader going
jQuery(function($) {
  $('.listing-layout').css('min-height', '1000px');
});


//View change - Ajaxify the change between grid and list on Property Listings page
jQuery(function($) {
  $('.view-type').on('click', 'a', function(e){

    $(this).toggleClass('active');
    $(this).siblings().toggleClass('active');

    e.preventDefault();
    var link = $(this).attr('href');

    $('.list-container').fadeOut(500, function(){
      $('.loader').css('display', 'block');

      $('.listing-layout').toggleClass('property-grid');

      if(link!=window.location){
        window.history.pushState({path:link},'',link);
      }

      function gup( name )
      {
        name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
        var regexS = "[\\?&]"+name+"=([^&#]*)";
        var regex = new RegExp( regexS );
        var results = regex.exec( window.location.href );
        if( results == null )
          return null;
        else
          return results[1];
      }

      var view_param = gup( 'view' );

      $(".pagination a").attr('href', function(i,a){
        return a.replace( /(view=)[a-z]+/ig, '$1'+ view_param );
      });

      //newpageurl = window.location.search.replace("?", "").split("&")[0];

      //alert(newpageurl);

      $(this).load(link + ' .list-container > *', function() {
        $('.loader').css('display', 'none');
        $(this).fadeIn(500);
      });
    });

  });
});