/*
 * jQuery Bookmarklet - version 1.0
 * Originally written by: Brett Barros
 * With modifications by: Paul Irish
 *
 * If you use this script, please link back to the source
 *
 * Copyright (c) 2010 Latent Motion (http://latentmotion.com/how-to-create-a-jquery-bookmarklet/)
 * Released under the Creative Commons Attribution 3.0 Unported License,
 * as defined here: http://creativecommons.org/licenses/by/3.0/
 *
 */

window.bookmarklet = function(opts) {
    fullFunc(opts);
};

// These are the styles, scripts and callbacks we include in our bookmarklet:
window.bookmarklet({

    css: ['http://www.site.com/your.css'],
    js: ['https://code.jquery.com/ui/1.11.1/jquery-ui.min.js'],
    //js: ['http://bluecoast.international/wp-content/themes/realhomes-child_v2/js/bookmarklet_extend.js'],
    // jqpath : 'myCustomjQueryPath.js', <-- option to include your own path to jquery
    ready: function() {

      // The meat of your jQuery code goes here

      var myCSS, myStyleNode, myHTML, myHTMLNode;
	    var sayHello;
	    var closeBkmrk;

	    if (!document.getElementById('bookmarking')) {
		    /* add the css */
		    myCSS  = '#bookmarking, #bookmarking *{font-family:Courier,"Courier New",sans-serif;color:#333;line-height:1.5em;font-size:15px;margin:0;padding:0;text-shadow:none;}';
		    myCSS += '#bookmarking {z-index:10000;position:absolute;top:8%;right:5%;background-color:#fff;box-shadow: -10px 10px 10px rgba(51, 51, 51, 0.6);}';
		    myCSS += '#bookmarking #closeButton {cursor:pointer;color: white;background: black;position: absolute;right: -14px;top: -14px;padding: 4px 10px;border-radius: 100px;border: 3px solid white;font-family: arial;}';
		    myCSS += '#bookmarking .c {width:350px; padding:20px;}';
		    myCSS += '#bookmarking h1 {font-size:20px; margin-bottom:0.5em;color:#0080C0}';
		    myCSS += '#bookmarking p {margin-bottom:0.5em;}';
		    myCSS += '#bookmarking #bb {color:white;margin-bottom:15px;padding:3px;width:100%;}';
		    myCSS += '#bookmarking .apb {clear:both;display:block;transition:all .3s ease-in-out;}';
		    myCSS += '#bookmarking .apb:hover {background:#eaeaea;}';
		    myCSS += '#bookmarking img {float:left;margin:0 10px 10px 0;width:33%;}';

		    /* then insert it */
		    myStyleNode = document.createElement('style');
		    myStyleNode.innerHTML = myCSS;
		    document.body.appendChild(myStyleNode);

		    /* build the HTML element */
		    myHTML  = '<div id="closeButton">X</div>';
		    myHTML += '<div class="c">';
		    myHTML += '<h1>Your Bookmarks</h1>';
		    myHTML += '<button id="bb" class="btn btn-success">Click to add property</button>';

		    if (localStorage.getItem('properties') !== null ) {
		    	var a = localStorage.getItem('properties');
		    	var jsonData = JSON.parse(a);

	    		$.each(jsonData, function(key, value) {
		    		myHTML += '<a href="' + window.location.protocol + '//' + window.location.host + '/' + window.location.pathname + '" class="apb">';
		    		myHTML += '<img src="' + value.image + '" alt="' + value.title + '">';
		    		myHTML += '<p>' + value.title + '</p>';
		    		myHTML += '<div class="property_ref">Ref: ' + value.id + '</div>';
		    		myHTML += '<div class="property_price">Price: ' + value.price + '</div>';
		    		myHTML += '</a>';
	    		});
		    }

		    myHTML += '</div>';


		    /* and create the node */
		    myHTMLNode = document.createElement('div');
		    myHTMLNode.id = 'bookmarking';
	    	myHTMLNode.innerHTML = myHTML;

	    } else {
	    	$('#bookmarking').fadeIn(500);
	    }

		    /* add js functionality to it */
		    sayHello = function() {
		    	var theSite = window.location.host;
		    	//var theLocation = window.location.pathname.split( '/' );

		    	if ( theSite === 'bluecoast.international' ) {
		    		var pageTitle = document.getElementById("galleryScroll").title;
		    		var favImage = document.getElementById("mainImg").src;
		    		var propId = document.getElementById("propId").innerText || document.getElementById("propId").textContent;
		    		var propPrice = document.getElementById("propertyPrice").innerText || document.getElementById("propertyPrice").textContent;

		    		var test = localStorage.getItem('properties');
		    		var obj = [];

		    		if ( test ) {
		    			obj = JSON.parse(test);
		    		}

		    		obj.push({ "title": pageTitle, "image": favImage, "id": propId, "price": propPrice });
		    		localStorage.setItem('properties',JSON.stringify(obj));

		    		console.log(localStorage.getItem('properties'));
		    	}
		    };

		    closeBkmrk = function() {
		    	var bkmrking = document.getElementById('bookmarking');
		    	$(bkmrking).fadeOut(500);
		    };

		    $('#bookmarking').draggable();

		    if (myHTMLNode.addEventListener) {
		    	myHTMLNode.querySelector('#closeButton').addEventListener('click', closeBkmrk, false);
		      myHTMLNode.querySelector('#bb').addEventListener('click', sayHello, false);
		    } else if (el.attachEvent) {
		    	myHTMLNode.querySelector('#closeButton').attachEvent('onclick', closeBkmrk);
		      myHTMLNode.querySelector('#bb').attachEvent('onclick', sayHello);
		    }

		    /* to add MORE scripts */
				// var myScriptNode=document.createElement('script');
				// myScriptNode.setAttribute('src','http://some.js/file.more.js');
				// document.head.appendChild(myScriptNode);

		    /* inject the node, with the event attached */
		    document.body.appendChild(myHTMLNode);
    }
});

function fullFunc(opts){

  // User doesn't have to set jquery, we have a default.
  opts.jqpath = opts.jqpath || "http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js";

  function getJS(jsfiles){

		// Check if we've processed all of the JS files (or if there are none)
		if (jsfiles.length === 0) {
			opts.ready();
			return false;
		}

    // Load the first js file in the array
    $.getScript(jsfiles[0],  function(){
      // When it's done loading, remove it from the queue and call the function again
      getJS(jsfiles.slice(1));
    });
  }

  // Synchronous loop for css files
  function getCSS(csfiles){
    $.each(csfiles, function(i, val){
      $('<link>').attr({
        href: val,
        rel: 'stylesheet'
      }).appendTo('head');
    });
  }

	function getjQuery(filename) {

		// Create jQuery script element
		var fileref = document.createElement('script');
		fileref.type = 'text/javascript';
		fileref.src =  filename;

		// Once loaded, trigger other scripts and styles
		fileref.onload = function(){

			getCSS(opts.css); // load CSS files
			getJS(opts.js); // load JS files

		};

		document.body.appendChild(fileref);
	}

	getjQuery(opts.jqpath); // kick it off

}