// Parallax Effect without Plugin
jQuery(window).scroll(function() {
  var x = jQuery(this).scrollTop(); // Pos
  var r = 10; // Rate
  jQuery('#slide-0 .bcg').css('background-position', 'center ' + parseInt(x/r) + 'px'); // Switch x polarity to reverse direction
});