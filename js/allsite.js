// Header - Slide out card for the phone icon
/*
jQuery(document).ready(function($) {
	$('.contact-number').hover(
		function() {
			jQuery('.contact-number div.number').show('slide', {direction: 'right'}, 400);
		},
		function() {
			jQuery('.contact-number div.number').hide('slide', {direction: 'right'}, 400);
		}
	);
});
*/

//Pagination - Ajaxify the Pagination
jQuery(function($) {
  $('.property-items').on('click', '.pagination a', function(e){

    e.preventDefault();
    var link = $(this).attr('href');

    $('.property-items-container').fadeOut(500, function(){
			$('.loader').css('display', 'block');
      $(this).load(link + ' .property-items-container > *', function() {
        $('.loader').css('display', 'none');
        $(this).fadeIn(500);
      });
    });

    $('.pagination').fadeOut(500, function(){
			$(this).load(link + ' .pagination > a', function(){
				$(this).fadeIn(500);
			});
    });

  });
});

// Pagination - Keep the height of the elements while ajax loader going
jQuery(function($) {
	var itemContainer = $('.property-items-container').innerHeight();

	$('.property-items-container').css('height', itemContainer);
});


// Slidebar sliding nav bar
jQuery(document).ready(function($) {
	$('#menu-main-nav').clone().prependTo('.sb-right');

	$.slidebars();
	var mySlidebars = new $.slidebars();
	$('#mobile-button').on('click', function() {
    mySlidebars.toggle('right');
  });

  /* Enable swipe if mobile device
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		$("body").swipe( {
			swipeRight:function(event, direction, distance, duration, fingerCount) {
				var active = $('.sb-sidebar').hasClass('sb-active');
				if ( direction === 'right' ) {
					$('#mobile-button').trigger('click');
				}
			},
			swipeLeft:function(event, direction, distance, duration, fingerCount) {
				var active = $('.sb-sidebar').hasClass('sb-active');
				if ( direction === 'left' ) {
					$('#mobile-button').trigger('click');
				}
			},
			threshold:0,
			fingers:'all'
		});
	} */
});


// Property page - Place featured image into content on under 1080px width
jQuery(document).ready(function($) {
	if (window.innerWidth <= 1080) {
		$('article.property-item .content').prepend($('.fixed-box figure'));
		$('#slide-0 .fixed-box').addClass('clearfix');
	}

	$(window).on('resize', function(){
    if (window.innerWidth <= 1080) {
			$('article.property-item .content').prepend($('.fixed-box figure'));
			$('#slide-0 .fixed-box').addClass('clearfix');
		} else {
			$('#slide-0 .fixed-box').prepend($('article.property-item .content figure'));
			$('#slide-0 .fixed-box').removeClass('clearfix');
		}
	});


	if (window.innerWidth < 768) {
		$('#footer.container > .row').prepend($('#footer.container > .row > .span6'));
	}

	$(window).on('resize', function(){
    if (window.innerWidth < 768) {
			$('#footer.container > .row').prepend($('#footer.container > .row > .span6'));
		} else {
			$('#footer.container > .row > .span6').insertAfter($('#footer.container > .row > .span3:first'));
		}
	});

	if (window.innerWidth < 615) {
		$('#pheader').addClass('shrink');
		$('#pheader .main-menu-holder').addClass('sticky');
		$('#slide-0 .the-content-box').prepend($('#slide-0 .property-item figure'));
		$('#slide-0 .the-content-box').prepend($('#slide-0 h1.page-title span'));
	}

	$(window).on('resize', function(){
    if (window.innerWidth < 615) {
			$('#pheader').addClass('shrink');
			$('#pheader .main-menu-holder').addClass('sticky');
			$('#slide-0 .the-content-box').prepend($('#slide-0 .property-item figure'));
			$('#slide-0 .the-content-box').prepend($('#slide-0 h1.page-title span'));
		} else {
			$('#pheader').removeClass('shrink');
			$('#pheader .main-menu-holder').removeClass('sticky');
			$('#slide-0 .property-item figure').insertBefore($('#slide-0 .the-content-box'));
			$('#slide-0 h1.page-title').prepend($('#slide-0 .the-content-box span'));
		}
	});
});

//Homepage Newsletter Form inputs
jQuery(document).ready(function($){
	$('.mymail-form .submit-button').addClass('btn btn-success');
});