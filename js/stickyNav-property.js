// Sticky header
jQuery(document).ready(function($) {

    var stickyNavTop = $('.main-menu-holder').offset().top;

    var stickyNav = function(){
        var scrollTop = $('main.single-property').scrollTop();

        if ($('.single-property').length) {
            if (scrollTop > 0) {
                $('#header').fadeOut(200, function() {
                    $('.header-wrapper').addClass('shrink');
                    $('.main-menu-holder').addClass('sticky');
                });
            } else {
                $('.header-wrapper').removeClass('shrink');
                $('.main-menu-holder').removeClass('sticky');
                $('#header').delay(160).fadeIn(400);
            }
        } else {
            if (scrollTop > stickyNavTop) {
                $('.main-menu-holder').addClass('sticky');
            } else {
                $('.main-menu-holder').removeClass('sticky');
            }
        }
    };

    stickyNav();

    $('main.single-property').scroll(function() {
        stickyNav();
    });
});