 /**
 * This demo was prepared for you by Petr Tichy - Ihatetomatoes.net
 * Want to see more similar demos and tutorials?
 * Help by spreading the word about Ihatetomatoes blog.
 * Facebook - https://www.facebook.com/ihatetomatoesblog
 * Twitter - https://twitter.com/ihatetomatoes
 * Google+ - https://plus.google.com/u/0/109859280204979591787/about
 * Article URL: http://ihatetomatoes.net/simple-parallax-scrolling-tutorial/
 */

( function( $ ) {
	
	// Setup variables
	var $window = $(window);
	var $slide = $('.homeSlide');
	var $body = $('body');
 

	function adjustWindow(){
		
		// Init Skrollr
		var s = skrollr.init({
			forceHeight: false,
			smoothScrolling: true,
			render: function(data) {
					//Debugging - Log the current scroll position.
					//console.log(data.curTop);
			}
		});

		// Get window size
		var winH = $window.height();

		// Keep minimum height 550
		if(winH <= 550) {
			winH = 550;
		}

		// Resize our slides
		$slide.height(winH);

		// Refresh Skrollr after resizing our sections
		s.refresh($('.homeSlide'));
	}

	// Resize sections
	adjustWindow();

} )( jQuery );



// Fill search form with + if no data is entered
jQuery(function($) {
	$('input[type="submit"]').click(function() {
		var search = $('input[type="search"]');
		if( search.val() === '' ) {
			search.val(' ');
		}
	});
});