(function($) {
  var Link = $.noUiSlider.Link;

  function commaSeparateNumber(val){
    val = val.toString().replace(/,/g, ''); //remove existing commas first
    var valSplit = val.split('.'); //then separate decimals

    while (/(\d+)(\d{3})/.test(valSplit[0].toString())){
      valSplit[0] = valSplit[0].toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }

    if(valSplit.length === 2){ //if there were decimals
      val = valSplit[0] + "." + valSplit[1]; //add decimals back
    }else{
      val = valSplit[0]; }

    return val;
  }

  $("#slider-range").noUiSlider({
    start: [ 150000, 1000000 ],
    behaviour: 'drag-tap',
    connect: true,
    range: {
      'min': [ 50000, 10000 ],
      '10%': [ 100000, 50000 ],
      '50%': [ 1000000, 500000 ],
      'max': [ 10000000 ]
    },
    serialization: {
      lower: [
        new Link({
          target: $('#min-price'),
          format: {
            decimals: 0
          }
        }),
        new Link({
          target: function(value){
            $('.noUi-handle-lower').attr('data-value', '\u20AC' + commaSeparateNumber(value));
          },
          format: {
            decimals: 0
          }
        })
      ],
      upper: [
        new Link({
          target: $('#max-price'),
          format: {
            decimals: 0
          }
        }),
        new Link({
          target: function(value){
            $('.noUi-handle-upper').attr('data-value', '\u20AC' + commaSeparateNumber(value));
          },
          format: {
            decimals: 0
          }
        })
      ]
    }
  });
})(jQuery);