<div class="span6 ">
    <article class="property-item clearfix">

        <h4><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h4>

        <?php
            $the_images = show_property_data('gallery-images');
            if( !empty($the_images) ){
                ?>
                <figure>
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                        <div class="home-property-image" style="background: url(<?php echo $the_images[0]['url']; ?>); width: 244px; height: 180px; display: block; background-size: 100%; background-repeat: no-repeat;">

                        </div>
                    </a>
                    <figcaption>
                        <?php
                            echo show_property_data('status');
                        ?>
                    </figcaption>
                </figure>
            <?php
        ?>

        <div class="detail">
            <h5 class="price">
                <?php
                    echo show_property_data('price');
                ?>
            </h5>
            <p>
                <?php
                    $description = show_property_data('description');
                    echo substr($description, 0, 100) . '...';
                ?>
            </p>
            <a class="more-details" href="<?php the_permalink() ?>"><?php _e('More Details ','framework'); ?><i class="fa fa-caret-right"></i></a>
        </div>

        <div class="property-meta">
            <?php get_template_part('property-details/property-metas'); ?>
        </div>

    </article>
</div>