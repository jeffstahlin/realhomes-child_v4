<div class="call-to-action mobile">
  <div class="cta-button-box">
      <div class="call-icon">
          <a href="tel:+34952345678">
            <i class="fa fa-phone"></i><br>
          </a>
      </div>
      <div class="email-icon">
          <i class="fa fa-envelope"></i><br>
      </div>
      <div class="bookmark-icon">
          <i class="fa fa-bookmark"></i><br>
      </div>
  </div>
</div>