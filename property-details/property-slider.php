<?php
$size = 'property-detail-slider-image';
$properties_images = rwmb_meta( 'REAL_HOMES_property_images', 'type=plupload_image&size='.$size, $post->ID );

$theImages = show_property_data('images');

if( !empty($theimages) ){
    ?>
        <?php $theImages; ?>
    <?php
    if(has_post_thumbnail()){
        ?>
        <div id="property-featured-image" class="clearfix only-for-print">
            <?php
            $image_id = get_post_thumbnail_id();
            $image_url = wp_get_attachment_url($image_id);
            echo '<a href="'.$image_url.'" class="'.get_lightbox_plugin_class() .'" '.generate_gallery_attribute().'>';
            echo '<img src="'.$image_url.'" alt="'.get_the_title().'" />';
            echo '</a>';
            ?>
        </div>
        <?php
    }
}elseif(has_post_thumbnail()){
    ?>
    <div id="property-featured-image" class="clearfix">
        <?php
            $image_id = get_post_thumbnail_id();
            $image_url = wp_get_attachment_url($image_id);
            echo '<a href="'.$image_url.'" class="'.get_lightbox_plugin_class() .'" '.generate_gallery_attribute().'>';
            echo '<img src="'.$image_url.'" alt="'.get_the_title().'" />';
            echo '</a>';
        ?>
    </div>
    <?php
}
?>