<?php
$display_similar_properties = get_option('theme_display_similar_properties');
if( $display_similar_properties == 'true' ){
    global $post;

    $getPrice = show_property_data('price');
    $getPrice = (float) priceToFloat($getPrice);
    $getPricePercent = (10/100) * $getPrice;
    $getMinPrice = $getPrice - $getPricePercent;
    $getMaxPrice = $getPrice + $getPricePercent;
    $getTown = show_property_data('town');
    $getType = show_property_data('type');
    $getProvince = show_property_data('province');

    // $sql = "select property_id from blu_properties where type like '%$getType%' or province like '%$getProvince%' order by last_updated desc limit 3";
    $sql = "select property_id, price from blu_properties where price between $getMinPrice and $getMaxPrice and town like '%$getTown%' and type not like '%plot%' limit 3";
    $the_query = $wpdb->get_results($sql);

    $propArray = array();
    foreach ($the_query as $tq) {
        $propIdQuery = "select ID from blu_posts where post_excerpt like '%$tq->property_id%'";
        $propIdResult = $wpdb->get_row($propIdQuery);
        $propArray[] = $propIdResult->ID;
    }
    //$propArray = implode(', ', $propArray);

    if( !empty($the_query) ) {
        $query = new WP_Query( array( 'post_type' => 'property', 'post__in' => $propArray ) );
        if( $query->have_posts() ) : ?>
            <section class="listing-layout property-grid">
                <div class="list-container clearfix">
                    <h3><?php _e('Similar Properties','framework'); ?></h3>
                    <?php
                        
                        while ( $query->have_posts() ) :
                            $query->the_post();

                            get_template_part('template-parts/property-for-grid');

                        endwhile;
                        wp_reset_query();
                    
                    ?>
                </div>
            </section>
        <?php endif;
    }
        
}
?>