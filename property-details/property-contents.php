<article class="property-item clearfix">

    <div class="wrap clearfix">
        <h4 class="title"><span><span><?php echo 'BC' . $post->ID; ?></span></h1>
        <h5 class="price">
            <span class="status-label">
                <?php echo show_property_data('status'); ?>
            </span>
            <span>
                <?php echo show_property_data('price'); ?>
                <small> - <?php echo show_property_data('type'); ?></small>
            </span>
        </h5>
    </div>

    <div class="property-meta clearfix">
        <?php
            /* Property Icons */
            get_template_part('property-details/property-metas');
        ?>
    </div>

    <div class="content clearfix col-lg-12">
        <div class="the-content-box" id="theContentBox">
        	<?php
        		$the_content = get_the_content();
        		$the_content = substr($the_content, 0, 800);
        		$utf8_the_content = \ForceUTF8\Encoding::fixUTF8($the_content);

        		$content = apply_filters( 'the_content', $utf8_the_content );
						$content = str_replace( ']]>', ']]&gt;', $content );

        		echo $content;
        	?>
        </div>
        <ul class="additional-details clearfix">
            <?php
                /* Property Features */
                $property_features = show_property_data('features');
                if(!empty($property_features)){
                    foreach ($property_features as $pf) {

                        if(!empty($pf['value']['uk'])) {

                            echo '<li><strong>'.$pf['name']['uk'].':</strong><span>' . $pf['value']['uk'] . '</span></li>';

                        }
                    }
                }
            ?>
        </ul>
    </div>

</article>