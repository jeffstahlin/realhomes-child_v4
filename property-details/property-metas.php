<?php
        $post_meta_data = show_property_data('metas');

        if( !empty($post_meta_data[0]) ) {
            $prop_size = $post_meta_data[0];
            echo '<span><i class="icon-area"></i>';
            echo $prop_size . ' m<sup>2</sup>';
            echo '</span>';
        }

        if( !empty($post_meta_data[1]) ) {
                $prop_bedrooms = floatval($post_meta_data[1]);
                $bedrooms_label = ($prop_bedrooms > 1)? __('Bedrooms','framework' ): __('Bedroom','framework');
                echo '<span><i class="icon-bed"></i>'. $prop_bedrooms .'&nbsp;'.$bedrooms_label.'</span>';
        }

        if( !empty($post_meta_data[2]) ) {
                $prop_bathrooms = floatval($post_meta_data[2]);
                $bathrooms_label = ($prop_bathrooms > 1)?__('Bathrooms','framework' ): __('Bathroom','framework');
                echo '<span><i class="icon-bath"></i>'. $prop_bathrooms .'&nbsp;'.$bathrooms_label.'</span>';
        }

        if( !empty($post_meta_data[3]) ) {
                $prop_garage = floatval($post_meta_data[3]);
                $garage_label = ($prop_garage > 1)?__('Garages','framework' ): __('Garage','framework');
                echo '<span><i class="icon-garage"></i>'. $prop_garage .'&nbsp;'.$garage_label.'</span>';
        }

  ?>