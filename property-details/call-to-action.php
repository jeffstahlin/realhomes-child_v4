<div class="call-to-action pop-up">
  <div class="cta-button-box">
      <div class="call-button action-content">
        <div class="call-action n1">
          <a href="skype:danlunt?call">
            <img src="/wp-content/themes/realhomes-child_v2/images/skype_icon.png" alt="<?php _e( 'Call on Skype', 'framework' ); ?>">
          </a>
          <div><?php _e( 'Call Us on Skype', 'framework' ) ?></div>
        </div>
        <div class="call-action n2">
          <div><?php _e( 'Or leave us your number and we\'ll call you', 'framework' ); ?></div>
          <?php echo do_shortcode( '[gravityform id="5" name="Call Us" title="false" description="false" ajax="true"]' ); ?>
        </div>
      </div>
      <div class="email-button action-content">
        <div class="email-action">
          <?php echo do_shortcode( '[gravityform id="6" name="Email Us" title="false" description="false" ajax="true"]' ); ?>
        </div>
      </div>
      <div class="bookmark-button action-content">
        <button class="btn btn-success" id="addBookmark">
          <?php _e( 'Bookmark this Property', 'framework' ); ?>
        </button>
        <button class="btn btn-danger clear-all">
          <?php _e( 'Clear All', 'framework' ); ?>
        </button>
        <section id="bookmarks_container">
          <ul id="bookmarks" class="bookmarks"></ul>
          <span class="page_navigation pagination"></span>
        </section>
      </div>
  </div>
</div>
<div class="call-to-action">
  <p><?php _e( 'Want to know more', 'framework' ); ?>?</p>
  <div class="cta-button-box">
      <div class="call-icon cta-icon">
        <i class="fa fa-phone" id="call-button"></i><br>
        <?php _e( 'Call Us', 'framework' ); ?>
      </div>
      <div class="email-icon cta-icon">
        <i class="fa fa-envelope" id="email-button"></i><br>
        <?php _e( 'Email Us', 'framework' ); ?>
      </div>
      <div class="bookmark-icon cta-icon">
        <i class="fa fa-bookmark" id="bookmark-button"></i><br>
        <?php _e( 'Bookmark', 'framework' ); ?>
      </div>
  </div>
</div>