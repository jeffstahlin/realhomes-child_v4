<!doctype html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html class="lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">

    <title><?php wp_title('|', true, 'right'); ?><?php bloginfo('name'); ?></title>


    <?php
    $favicon = get_option('theme_favicon');
    if( !empty($favicon) )
    {
        ?>
        <link rel="shortcut icon" href="<?php echo $favicon; ?>" />
        <?php
    }
    ?>

    <!-- Define a viewport to mobile devices to use - telling the browser to assume that the page is as wide as the device (width=device-width) and setting the initial page zoom level to be 1 (initial-scale=1.0) -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <!-- Style Sheet-->
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>"/>

    <!-- Pingback URL -->
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

    <!-- RSS -->
    <link rel="alternate" type="application/rss+xml" title="<?php bloginfo( 'name' ); ?>" href="<?php bloginfo( 'rss2_url' ); ?>" />
    <link rel="alternate" type="application/atom+xml" title="<?php bloginfo( 'name' ); ?>" href="<?php bloginfo( 'atom_url' ); ?>" />

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <?php
    // Google Analytics From Theme Options
    echo stripslashes(get_option('theme_google_analytics'));

    wp_head();
    ?>
</head>
<body <?php body_class(); ?> id="skrollr-body">
    <!-- Start Main Menu-->
        <div class="main-menu-holder sb-slide">
            <div>
                <nav class="main-menu">
                    <div class="sticky-logo">
                        <a href="<?php echo home_url(); ?>"  title="<?php bloginfo( 'name' ); ?>">
                            <img src="/wp-content/uploads/2014/04/sticky-logo.png">
                        </a>
                    </div>
                    <?php
                        wp_nav_menu( array(
                            'theme_location' => 'main-menu',
                            'menu_class' => 'clearfix'
                        ));
                    ?>
                    <div id="mobile-button">
                        <a id="responsive-menu-button" href="#mobile-menu">
                            Menu <i class="fa fa-list-ul"></i>
                        </a>
                    </div>
                </nav>
            </div>
        </div>
    <!-- End Main Menu -->

    <div id="sb-site">

        <!-- Start Header -->
        <div class="header-wrapper"
            <?php
                if( is_front_page() ) {
                    echo 'style="position: relative; width: 100%; top: 0; z-index: 1000;"';
                }
            ?>
        >

            <div class="container"><!-- Start Header Container -->

                <header id="header" class="clearfix">

                    <div id="header-top" class="clearfix">
                        <?php
                        /* WPML Language Switcher */
                        if(function_exists('icl_get_languages')){
                            $wpml_lang_switcher = get_option('theme_wpml_lang_switcher');
                            if($wpml_lang_switcher == 'true'){
                                do_action('icl_language_selector');
                            }
                        }

                        $header_phone = get_option('theme_header_phone');
                        if( !empty($header_phone) ){
                            ?>
                            <h2 class="contact-number">
                                <div class="number">
                                    <div class="click-to-call">Click to call</div>
                                    or phone us on <?php echo $header_phone ?>
                                </div>
                                <i class="fa fa-phone"></i>
                            </h2>
                            <?php
                        }

                        $header_email = get_option('theme_header_email');
                        if(!empty($header_email)){
                            ?>
                            <h2 id="contact-email">
                                <a href="mailto:<?php echo antispambot($header_email); ?>">
                                    <i class="fa fa-envelope"></i>
                                </a>
                            </h2>
                            <?php
                        }
                        ?>

                        <!-- Social Navigation -->
                        <?php  get_template_part('template-parts/social-nav') ;    ?>


                        <?php
                        $enable_user_nav = get_option('theme_enable_user_nav');
                        if( $enable_user_nav == "true" ){
                            ?>
                            <div class="user-nav clearfix">
                                <?php
                                if(is_user_logged_in()){

                                    $submit_url = get_option('theme_submit_url');
                                    $my_properties_url = get_option('theme_my_properties_url');

                                    if(!empty($submit_url)){
                                        ?><a href="<?php echo $submit_url; ?>"><i class="fa fa-plus-circle"></i><?php _e('Submit Property','framework'); ?></a><?php
                                    }

                                    if(!empty($my_properties_url)){
                                        ?><a href="<?php echo $my_properties_url; ?>"><i class="fa fa-th-list"></i><?php _e('My Properties','framework'); ?></a><?php
                                    }
                                    ?>
                                    <a href="/profile"><i class="fa fa-star"></i><?php _e('My Favourites','framework'); ?></a>
                                    <a class="last" href="<?php echo wp_logout_url( home_url() ); ?>"><i class="fa fa-sign-out"></i><?php _e('Logout','framework'); ?></a>
                                    <?php

                                }else{
                                    $theme_login_url = get_option('theme_login_url');
                                    if(!empty($theme_login_url)){
                                        ?>
                                        <a href="/login"><i class="fa fa-sign-in"></i><?php _e('Login','framework'); ?></a>
                                        <a class="last" href="/register"><?php _e('Register','framework'); ?></a>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                            <?php
                        }
                        ?>

                    </div>

                    <!-- Logo -->
                    <div id="logo">

                        <?php
                            $logo_path = get_option('theme_sitelogo');
                            if(!empty($logo_path)){
                                ?>
                                <a title="<?php  bloginfo( 'name' ); ?>" href="<?php echo home_url(); ?>">
                                    <img src="<?php echo $logo_path; ?>" alt="<?php  bloginfo( 'name' ); ?>">
                                </a>
                                <h2 class="logo-heading only-for-print">
                                    <a href="<?php echo home_url(); ?>"  title="<?php bloginfo( 'name' ); ?>">
                                        <?php  bloginfo( 'name' ); ?>
                                    </a>
                                </h2>
                                <?php
                            }else{
                                ?>
                                <h2 class="logo-heading">
                                    <a href="<?php echo home_url(); ?>"  title="<?php bloginfo( 'name' ); ?>">
                                        <?php  bloginfo( 'name' ); ?>
                                    </a>
                                </h2>
                                <?php
                            }
                        ?>
                    </div>

                </header>

            </div> <!-- End Header Container -->

        </div><!-- End Header -->
