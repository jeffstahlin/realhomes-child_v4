<!-- Start Footer -->
<footer id="footer-wrapper">

    <div id="footer" class="container">

        <div class="row">

            <div class="span3">
                <?php if ( ! dynamic_sidebar( __('Footer First Column','framework') )) : ?>
                <?php endif; ?>
            </div>

            <div class="span6">
                <?php get_template_part("template-parts/carousel_partners"); ?>
            </div>

            <div class="span3">
                <?php if ( ! dynamic_sidebar( __('Footer Fourth Column','framework') )) : ?>
                <?php endif; ?>
            </div>
        </div>

    </div>

    <!-- Footer Bottom -->
    <div id="footer-bottom" class="container">

        <div class="row">
            <div class="span6">
                <?php
                    $copyright_text = get_option('theme_copyright_text');
                    echo ( $copyright_text ) ? '<p class="copyright">'.$copyright_text.'</p>' : '';
                ?>
            </div>
            <div class="span6">
                <?php
                    $designed_by_text = get_option('theme_designed_by_text');
                    echo ( $designed_by_text ) ? '<p class="designed-by">'.$designed_by_text.'</p>' : '';
                ?>
            </div>
        </div>

    </div>
    <!-- End Footer Bottom -->

</footer><!-- End Footer -->

</div><!-- End sb-site div -->

<?php wp_footer(); ?>

<div class="sb-slidebar sb-right">
  
</div>

<?php get_template_part('property-details/call-to-action-mobile'); ?>

</body>
</html>